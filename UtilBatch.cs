using System;
using System.Data;
using System.Data.OleDb;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for UtilBatch.
	/// </summary>
	public class UtilBatch
	{
		public UtilBatch()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public static bool FolderChangePath(OleDbConnection cnBatch, String strFolder)
		{
			clsADONET cADO = new clsADONET(ref cnBatch, "SELECT * FROM [Folder] WHERE [Folder].[Folder_ID]=1;");
			if(cADO.dTbl.Rows.Count == 0)
			{
				DataRow dr = cADO.dTbl.NewRow();
				dr["Folder_ID"] = 1;
				dr["FolderName"] =  UtilFile.DirNoSlash(strFolder);
				cADO.dTbl.Rows.Add(dr);
			}
			else
			{
				cADO.dTbl.Rows[0]["FolderName"] = UtilFile.DirNoSlash(strFolder);
			}
			cADO.Update();
			cADO.Dispose();
			cADO = null;
			return true;
		}

		public static int TRCMinTRC_ID(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[TRC_ID]", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
			EndFunction:
				dRead.Close();
			cmd.Dispose();
			return intRet;
		}
				
		public static int TRCMaxTRC_ID(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[TRC_ID] DESC", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRCMaxFormNum(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[Form_Num] DESC", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["Form_Num"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRC_FormNum(OleDbConnection cnBatch, String strTRC)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strTRC + "';", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["Form_Num"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRC_ID(OleDbConnection cnBatch, String strTRC)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strTRC + "';", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}
	
		public static String DocumentAddImages(OleDbConnection cnBatch, ArrayList arrTif,
												String strImageFolder)
		{
			//add images to a blank document table
			//arrTif is a sorted list of JUST IMAGE NAMES .. not the full paths
			clsADONET cADO = new clsADONET(ref cnBatch);

			if(!cADO.FillDataTable("SELECT * FROM [Document];"))
			{
				cADO.Dispose();
				cADO = null;
				return "ERROR : Unable to read document table";
			}

			if(cADO.dTbl.Rows.Count > 0)
			{
				cADO.Dispose();
				cADO = null;
				return "ERROR: Images already exist in document table";
			}
			String strFileDate;
			String strTif;
			for(int i=0; i<arrTif.Count; i++)
			{
				strTif = UtilFile.DirWithSlash(strImageFolder) + arrTif[i].ToString();
				if(UtilFile.FileIsThere(strTif))
				{
					strFileDate = System.IO.File.GetLastWriteTime(strTif).ToString();
				}else
				{
					strFileDate = System.DateTime.Now.ToString();
				}
				DataRow dr = cADO.dTbl.NewRow();
				dr["ImageName"] = arrTif[i].ToString();
				dr["Image_ID"] = i + 1;
				dr["IsAttachment"] = false;
				dr["FileDate"] = strFileDate;
				dr["Document_ID"] = 1;
				dr["Folder_ID"] = 1;
				dr["Page_ID"] = 0;
				dr["iImprinterNo"] = 0;
				cADO.dTbl.Rows.Add(dr);
			}

			cADO.Update();
			cADO.Dispose();
			cADO = null;
			return "";
		}

		public static bool InsertFMCFromFile(String strBatch, String strFMC)
		{
			String strTemp;
			int intMaxTRC;
			int intMaxFormNum;
			
			if(!UtilFile.FileIsThere(strBatch) || !UtilFile.FileIsThere(strFMC))
			{
				return false;
			}
			//System.IO.MemoryStream ms = new System.IO.MemoryStream();
			System.IO.StreamReader sr = new System.IO.StreamReader(strFMC);
			System.IO.BinaryReader br = new System.IO.BinaryReader(sr.BaseStream);
			System.IO.FileInfo fInf = new System.IO.FileInfo(strFMC);
			int intLength = Convert.ToInt32(fInf.Length);

			byte[] byteBuff = br.ReadBytes(intLength);
			clsADONET cADO = new clsADONET(strBatch, true);

			intMaxTRC = TRCMaxTRC_ID(cADO.cnConn);
			intMaxFormNum = TRCMaxFormNum(cADO.cnConn);
			String SQL = clsADONET.GetSQL("TRC_MAIN");
			cADO.FillDataTable(SQL);

			strTemp = UtilFile.FileFromDir(strFMC);
			strTemp = UtilFile.NameMinusExtension(strTemp);

			DataRow dr = cADO.dTbl.NewRow();
			dr["TRC_NAME"] = strTemp;
			dr["TRC_BLOB"] = byteBuff;
			dr["TRC_ID"] = intMaxTRC + 1;
			dr["Form_Num"] = intMaxFormNum + 1;
			dr["Image_DPI"] = 300;
			dr["IsDocStart"] = true;
			cADO.dTbl.Rows.Add(dr);

			cADO.Update();
			cADO.Dispose(true);

			br.Close();
			sr.Close();
			return true;
		}

		public static string InsertFMCFromWorkflow(OleDbConnection cnWorkflow, 
			ref OleDbConnection cnBatch, String strFMC)
		{
			return InsertFMCFromWorkflow(cnWorkflow, ref cnBatch, strFMC, true);
		}
		public static string InsertFMCFromWorkflow(OleDbConnection cnWorkflow, 
			ref OleDbConnection cnBatch, String strFMC, bool boolCopyVersion)
		{
			//add the FMC and also copy over forms table junk
			int intTRC;
			int intFormNum;
			//bool boolRet = false;
			string strRet = "";

			intTRC = TRCMaxTRC_ID(cnBatch) + 1;
			
			intFormNum = TRC_FormNum(cnWorkflow, strFMC);
			if(intFormNum < 0)
			{
				//if the FMC isn't there, we can't insert it
				return "ERROR";
			}
			
			String SQL = "SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strFMC + "';";
			clsADONET cADO = new clsADONET(ref cnWorkflow);
		
			if(!cADO.FillDataTable(SQL))
			{	strRet = "ERROR"; goto EndFunction;	}			
						
			if(cADO.dTbl.Rows.Count != 1)
			{	strRet = "ERROR"; goto EndFunction;	}
			
			byte[] byteBuff = (byte[])cADO.dTbl.Rows[0]["TRC_BLOB"];
			
			string strVersion = "";
			
			if(boolCopyVersion && UtilDB.ColumnIsThere(cnWorkflow, "TRC_MAIN", "VERSION"))
				strVersion = cADO.dTbl.Rows[0]["Version"].ToString();
						
			cADO.Dispose();
			//Console.WriteLine(byteBuff.Length.ToString());

			OleDbDataAdapter da1 = new OleDbDataAdapter(SQL, cnBatch);
			OleDbCommandBuilder cb1 = new OleDbCommandBuilder(da1);
			DataSet ds1 = new DataSet();
			da1.Fill(ds1);
			
			//cADO = new clsADONET(ref cnBatch);

			//			if(!cADO.FillDataTable(SQL))
			//			{	goto EndFunction;	}			
			//						
			//			if(cADO.dTbl.Rows.Count != 0)
			//			{	goto EndFunction;	}

			DataRow dr = ds1.Tables[0].NewRow();
			dr["TRC_NAME"] = strFMC;
			dr["TRC_BLOB"] = byteBuff;
			dr["TRC_ID"] = intTRC;
			dr["Form_Num"] = intFormNum;
			dr["Image_DPI"] = 300;
			dr["IsDocStart"] = true;

			if(boolCopyVersion)
			{
				if(dr.Table.Columns.Contains("Version"))
					dr["Version"] = strVersion;
				else
					return "ERROR - missing 'Version' column in batch";
			}

			//cADO.dTbl.Rows.Add(dr);
			ds1.Tables[0].Rows.Add(dr);
			da1.Update(ds1);
			
			da1.Dispose();
			ds1.Dispose();
			cb1.Dispose();
			
			//cADO.Update();
			//cADO.Dispose();
			//cADO.cnConn = null;
			//now copy over the forms table data
			cADO = new clsADONET(ref cnWorkflow);
			//if(!cADO.FillDataTable("SELECT * FROM [FORMS_MASTER] WHERE [FORMS_MASTER].[FormType]=" + intFormNum + " AND [FORMS_MASTER].[ExcludeZone]<>-1;"))
			if(!cADO.FillDataTable("SELECT * FROM [FORMS_MASTER] WHERE [FORMS_MASTER].[FormType]=" + intFormNum + " AND [FORMS_MASTER].[ExcludeZone] = 0;"))
			{
				strRet = "ERROR";
				goto EndFunction;
			}
			
			DataTable dTbl = cADO.dTbl;
			cADO.Dispose();
			
			//cADO = new clsADONET(cnBatch);
			//cADO.FillDataTable("SELECT * FROM [Forms] WHERE [Forms].[FormType]=" + intFormNum + ";");

			SQL = "SELECT * FROM [Forms] WHERE [Forms].[FormType]=" + intFormNum + ";";
			OleDbDataAdapter da = new OleDbDataAdapter(SQL, cnBatch);
			OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
			cb.QuotePrefix = "[";
			cb.QuoteSuffix = "]";

			DataSet dSet = new DataSet();
			da.Fill(dSet);
			
			DataRow drNew;
			foreach(DataRow drRow in dTbl.Rows)
			{
				drNew = dSet.Tables[0].NewRow(); //cADO.dTbl.NewRow();
				foreach(DataColumn dc in dTbl.Columns)
				{
					if(drNew.Table.Columns.IndexOf(dc.ColumnName) >= 0)
					{
						drNew[dc.ColumnName] = drRow[dc.ColumnName];
					}
				}

				//default values
				//drNew["BOOLMultiline"] = false;
				drNew["FontSize"] = 0;
				drNew["NeuralClassifier"] = 0;

				dSet.Tables[0].Rows.Add(drNew);
			}

			//cADO.Update();

			//			cnBatch.Close();
			//			cnBatch.Dispose();
			//			
			//			if(cnBatch.State == ConnectionState.Open)
			//			{
			//				return false;
			//			}
			//			
			//			return true;

			da.Update(dSet);
			da.Dispose();
			dSet.Dispose();
			cb.Dispose();
			//
			//			da.Dispose();
			//			dSet.Clear();
			//			dSet.Dispose();
			//			cb.Dispose();
			//			cADO.Dispose();
			//			cADO = null;
			//		
			//			drNew = null;
			//			dTbl.Clear();
			//			dTbl = null;
			//			cb = null;
			//			dSet = null;
			//			da = null;
			//boolRet = true;
			EndFunction:
				//cADO.dTbl.Dispose();
				//cADO.Dispose();
				//cADO = null;
				//return boolRet;
				return strRet;
		}
		
//		public static bool InsertFMCFromWorkflow(OleDbConnection cnWorkflow, 
//			OleDbConnection cnBatch, String strFMC)
//		{
//			//add the FMC and also copy over forms table junk
//			int intTRC;
//			int intFormNum;
//			bool boolRet = false;
//
//			intTRC = TRCMaxTRC_ID(cnBatch) + 1;
//			intFormNum = TRC_FormNum(cnWorkflow, strFMC);
//			if(intFormNum < 0)
//			{
//				//if the FMC isn't there, we can't insert it
//				return false;
//			}
//			String SQL = "SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strFMC + "';";
//			clsADONET cADO = new clsADONET(cnWorkflow);
//
//			if(!cADO.FillDataTable(SQL))
//			{	goto EndFunction;	}			
//			
//			if(cADO.dTbl.Rows.Count != 1)
//			{	goto EndFunction;	}
//			
//			byte[] byteBuff = (byte[])cADO.dTbl.Rows[0]["TRC_BLOB"];
//			cADO.Dispose();
//			Console.WriteLine(byteBuff.Length.ToString());
//			
//			OleDbDataAdapter da1 = new OleDbDataAdapter(SQL, cnBatch);
//			OleDbCommandBuilder cb1 = new OleDbCommandBuilder(da1);
//			DataSet ds1 = new DataSet();
//			da1.Fill(ds1);
//			
//			DataRow dr = ds1.Tables[0].NewRow();
//			dr["TRC_NAME"] = strFMC;
//			dr["TRC_BLOB"] = byteBuff;
//			dr["TRC_ID"] = intTRC;
//			dr["Form_Num"] = intFormNum;
//			dr["Image_DPI"] = 300;
//			dr["IsDocStart"] = true;
//			
//			ds1.Tables[0].Rows.Add(dr);
//			da1.Update(ds1);
//			da1.Dispose();
//			ds1.Dispose();
//			cb1.Dispose();
//			
//			//now copy over the forms table data
//			cADO = new clsADONET(cnWorkflow);
//			if(!cADO.FillDataTable("SELECT * FROM [FORMS_MASTER] WHERE [FORMS_MASTER].[FormType]=" + intFormNum + " AND [FORMS_MASTER].[ExcludeZone]<>-1;"))
//			{
//				goto EndFunction;
//			}
//			
//			DataTable dTbl = cADO.dTbl;
//			cADO.Dispose();
//			
//			SQL = "SELECT * FROM [Forms] WHERE [Forms].[FormType]=" + intFormNum + ";";
//			OleDbDataAdapter da = new OleDbDataAdapter(SQL, cnBatch);
//			OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
//			cb.QuotePrefix = "[";
//			cb.QuoteSuffix = "]";
//
//			DataSet dSet = new DataSet();
//			da.Fill(dSet);
//			DataRow drNew;
//			foreach(DataRow drRow in dTbl.Rows)
//			{
//				drNew = dSet.Tables[0].NewRow(); //cADO.dTbl.NewRow();
//				foreach(DataColumn dc in dTbl.Columns)
//				{
//					if(drNew.Table.Columns.IndexOf(dc.ColumnName) >= 0)
//					{
//						drNew[dc.ColumnName] = drRow[dc.ColumnName];
//					}
//				}
//
//				//default values
//				//drNew["BOOLMultiline"] = false;
//				drNew["FontSize"] = 0;
//				drNew["NeuralClassifier"] = 0;
//
//				dSet.Tables[0].Rows.Add(drNew);
//			}
//
//			da.Update(dSet);
//			da.Dispose();
//			dSet.Dispose();
//			cb.Dispose();
//
////			cADO = new clsADONET(cnBatch);
////			cADO.FillDataTable("SELECT * FROM [Forms] WHERE [Forms].[FormType]=" + intFormNum + ";");
////			
////			DataRow drNew;
////			foreach(DataRow drRow in dTbl.Rows)
////			{
////				drNew = cADO.dTbl.NewRow();
////				foreach(DataColumn dc in dTbl.Columns)
////				{
////					if(drNew.Table.Columns.IndexOf(dc.ColumnName) >= 0)
////					{
////						drNew[dc.ColumnName] = drRow[dc.ColumnName];
////					}
////				}
////
////				//default values
////				//drNew["BOOLMultiline"] = false;
////				drNew["FontSize"] = 0;
////				drNew["NeuralClassifier"] = 0;
////				
////				cADO.dTbl.Rows.Add(drNew);
////			}
////			cADO.Update();
//			
//			boolRet = true;
//		EndFunction:
//			cADO.Dispose();
//			return boolRet;
//		}

		public static bool UserDefinedUpdateFromWorkflow(OleDbConnection cnWorkflow, 
			OleDbConnection cnBatch)
		{
			DataTable dTbl = null;
			int intTRC;
			bool boolRet = false;
			//now copy over the forms table data
			clsADONET cADO = new clsADONET(ref cnWorkflow);
			if(!cADO.FillDataTable("SELECT * FROM [UserDefined_Fields];"))
			{
				goto EndFunction;
			}
					
			dTbl = cADO.dTbl;
			cADO.Dispose();
			cADO = null;

			//just get ANY valid TRC for the batch
			intTRC = TRCMinTRC_ID(cnBatch);

			cADO = new clsADONET(ref cnBatch);
			cADO.FillDataTable("SELECT * FROM [UserDefined_Fields];");

			DataRow drNew;
			foreach(DataRow drRow in dTbl.Rows)
			{
				drNew = cADO.dTbl.NewRow();
				foreach(DataColumn dc in dTbl.Columns)
				{
					if(dc.ColumnName.ToUpper() != "TRC_ID")
					{
						drNew[dc.ColumnName] = drRow[dc.ColumnName];
					}
					else
					{
						drNew[dc.ColumnName] = intTRC;
					}
				}
				cADO.dTbl.Rows.Add(drNew);
			}
			cADO.Update();
			
			boolRet = true;
		EndFunction:	
			if(dTbl != null)
			{
				dTbl.Clear();
			}
			cADO.Dispose();
			cADO = null;
			return boolRet;
		}
	}
}
