using System;
using System.Data.OleDb;
using System.Data;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsBatch.
	/// </summary>
	public class clsBatch
	{
		public String strBatch;
		public OleDbConnection cnBatch;

		public clsBatch(String strBatchPath)
		{
			// TODO: Add constructor logic here
			this.strBatch = strBatchPath;
			cnBatch = null;
		}

		public bool OpenBatch()
		{
			try
			{
				cnBatch = new OleDbConnection(UtilDB.GetConnStringAccess(strBatch));
				cnBatch.Open();
			}
			catch
			{
				return false;
			}
			return true;
		}

		public void CloseBatch()
		{
			try
			{
				cnBatch.Close();
			}
			catch { }
		}

		public String DoBatch()
		{
			if(!clsADONET.OleDBOpen(out cnBatch, strBatch))
			{
				clsDocuProgram.Log("Unable to open Batch : " + strBatch);
				return "ERROR";
			}

			cnBatch.Close();
			return "FINISHED";
		}

		public String CreateBatch(String strImageFolder, clsWorkflow cWorkflow, 
			String strOutPath, String strFMCs, String strTemplate, 
			String strImageLikeStr, bool boolErrorIfMultPageTif, 
			bool boolSplitMultipage, String strModBatchName, bool boolCheckImageCount,
			bool boolMatchName, bool boolCopyVersion, bool boolIncludeCoverSheetImgCnt)
		{
			//copy template over to target batch path
			//insert FMC's
			//add folder path
			//add images to document table
			//add user defined fields from workflow
			String strRet="";
			String strBatchName;
			if(strModBatchName == "")
			{
				strBatchName = UtilFile.FileFromDir(strImageFolder);
			}
			else
			{
				strBatchName = strModBatchName;
			}

			// if specified, check batchname and image folder name
			if(boolMatchName && (strBatchName != UtilFile.FileFromDir(strImageFolder)))
			{
				return "ERROR: Name mismatch - Batch name: " + strModBatchName + "  --  Image folder name: " + UtilFile.FileFromDir(strImageFolder);
			}
						
			if(strFMCs == "")
			{
				return "ERROR: No FMC's specified for folder : " + strImageFolder;
			}
			
			if(!UtilFile.DirIsThere(strOutPath))
			{	return "ERROR: OutPath not found : " + strOutPath;	}

			if(!UtilFile.DirIsThere(strImageFolder))
			{	return "ERROR: Folder not found : " + strImageFolder;	}

			int intFolderNameImageCount = 0;
			string strImageCount = UtilString.DelimFieldFromEnd(UtilFile.FileFromDir(UtilFile.DirNoSlash(strImageFolder)), '_', 1);
			if(UtilString.IsNumber(strImageCount))  // 1.4.15
			{
				
				if (boolCheckImageCount && Convert.ToInt32(strImageCount) > 9999)  //1.4.16 
				{
					// 1.4.15 The image count in the batch name is not to exceed 4 digits, meaning the image count of 9999 is the maximum limit
					return "ERROR: total image count (" + strImageCount + ") in batch name exceeds 4 digits (exceeding maximum limit of 9,999 images): " + strImageFolder;
				}
				intFolderNameImageCount = Convert.ToInt32(strImageCount);
			} 
			
			String strErr = "";

			ArrayList arrTif = new ArrayList();
			clsDirectory cDir = new clsDirectory(strImageFolder);
			cDir.Populate(false);

			if(!cDir.GetArrFile(ref arrTif, "tif", false))
			{
				strErr = "ERROR: Unable to read directory : " + strImageFolder;
			}
			else if (cDir.checkSubDirectories())
			{	
				strErr = "ERROR: Subdirectories with Tif Images found! : " + strImageFolder;
			}
			else if(arrTif.Count == 0)
			{
				strErr = "ERROR: No .Tif files found in directory : " + strImageFolder;
			} 
			else if (arrTif.Count > 9999) // 1.4.15 limit the maximum number of images in a single batch to 9,999 images
			{
				strErr = "ERROR: Number of .Tif files found in batch folder (" + arrTif.Count + ") exceeds maximum limit of 9,999 images: " + strImageFolder;
            }
			else if((boolCheckImageCount && !boolIncludeCoverSheetImgCnt && arrTif.Count != intFolderNameImageCount + 1) || 
				(boolCheckImageCount && boolIncludeCoverSheetImgCnt && arrTif.Count != intFolderNameImageCount)) //1.4.12 IncludeCoverSheet option for CheckImageCount functionality
			{
				strErr = "ERROR: Image count in folder " + strImageFolder + " does not equal image count in batch name.";
			}
			else if(strImageLikeStr != "" && !CheckArrTif(arrTif, strImageLikeStr))
			{	
				strErr = "ERROR: Invalid .tif name/s in folder : " + strImageFolder;		
			}
			else
			{
				strBatch = UtilFile.DirWithSlash(strOutPath) + strBatchName + ".mdb";
				if(!UtilFile.OpenFileCopy(strTemplate, strBatch, false))
				{	
					return "ERROR: Unable to copy batch template '" + strTemplate + "' over to '" + strBatch + "'...(file may already exist)";
				}

				clsADONET.OleDBOpen(out cnBatch, strBatch);

				//STEP 3 now we have to insert FMC's
				ArrayList arrFMC;
				arrFMC = UtilString.Split(strFMCs, "|");

				foreach(String strFMC in arrFMC)
				{
					string strTemp = UtilBatch.InsertFMCFromWorkflow(cWorkflow.cnworkflow, ref cnBatch, strFMC, boolCopyVersion);
					if(strTemp.ToUpper().StartsWith("ERROR"))
					{	
						strRet = "ERROR: Unable to add FMC : " + strFMC + " to : " + strBatch + " MESSAGE: " + strTemp;	
						goto EndFunction;
					}
				}
				
				//STEP 4 add folder path
				UtilBatch.FolderChangePath(cnBatch, strImageFolder);

				if(boolSplitMultipage)
				{
					arrTif = UtilTiff.SplitTiffDirectory(strImageFolder, UtilFile.DirWithSlash(strImageFolder) + "Single");
					strImageFolder = UtilFile.DirWithSlash(strImageFolder) + "Single";
					UtilBatch.FolderChangePath(cnBatch, strImageFolder);
				}
				else if(boolErrorIfMultPageTif)
				{
					//check and make sure all the .tif's are not multipage
					for(int i=0; i<arrTif.Count; i++)
					{
						if(UtilTiff.TiffPageCount(arrTif[i].ToString()) != 1)
						{
							strRet = "ERROR: Multipage .tif/s in folder : " + strImageFolder;
							goto EndFunction;
						}
					}
				}
				for(int i=0; i<arrTif.Count; i++)
				{	arrTif[i] = UtilFile.FileFromDir(arrTif[i].ToString());	}

				//STEP 5 add images to document table
				
				//needs to be in order
				//arrTif.Sort();
				NumericComparer nc = new NumericComparer();
				arrTif.Sort(nc);
				strErr = UtilBatch.DocumentAddImages(cnBatch, arrTif, strImageFolder);
			}

			if(strErr != "")
			{	
				strRet = strErr;
				return strRet;
			}

			//STEP 6 finally add user defined fields
			UtilBatch.UserDefinedUpdateFromWorkflow(cWorkflow.cnworkflow, cnBatch);

		EndFunction:
			cnBatch.Close();
			cnBatch.Dispose();
			cnBatch = null;
			System.GC.Collect();
			return strRet;
		}

		public bool CheckArrTif(ArrayList arrTif, String strGroup)
		{
			//if any of the image names don't match up to specifications
			// return false
			String strLike = "";
			String strTemp;
			strLike = clsDocuProgram.cOptions.GetOptionParameter("ImageLikeStr", false, strGroup);
			if(strLike == "")
			{	return true;	}
			
			foreach(String str in arrTif)
			{
				strTemp = UtilFile.FileFromDir(str);
				if(!UtilString.LikeStr(strTemp.ToUpper(), strLike.ToUpper()))
				{	return false;	}
			}

			return true;
		}

	}
}
