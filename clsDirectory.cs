using System;
using System.Collections;
using System.IO;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsDirectory.
	/// </summary>
	public class clsDirectory 
	{
		public String strDirectory;
		public ArrayList arrFile;
		public ArrayList arrDirectory;

		public clsDirectory(String strDir)
		{
			// TODO: Add constructor logic here
			strDirectory = strDir;
			arrFile = new ArrayList();
			arrDirectory = new ArrayList();
		}

		public bool Populate()
		{
			return Populate(false);
		}

		public bool Populate(bool boolPopulateSubDirs)
		{
			Array strFiles;

			if(!UtilFile.DirIsThere(strDirectory))
			{
				return false;
			}
			
			strFiles = System.IO.Directory.GetFiles(strDirectory);

			clsDirectory cDir;
			clsFile cFile;

			foreach(String str in strFiles)
			{
				cFile = new clsFile(str);
				arrFile.Add(cFile);
			}

			strFiles = null;
			strFiles = System.IO.Directory.GetDirectories(strDirectory);

			foreach(String str in strFiles)
			{
				if(str != ".." && str != ".")
				{
					//we have a directory
					cDir = new clsDirectory(str);
					if(boolPopulateSubDirs)
					{
						
						cDir.Populate();
					}
					arrDirectory.Add(cDir);
				}
			}

			return true;
		}

		public bool GetArrFile(ref ArrayList arrF, String strExtension, bool boolSubDirs)
		{
			//'populate an array of matching files with all files from all directories (including sub dirs)
			foreach(clsFile cFile in arrFile)
			{
				if(strExtension == "")
				{	arrF.Add(cFile.strFile);	}
				else if(UtilString.LikeStr(cFile.strFile.ToUpper(), "*." + strExtension.ToUpper()))
				{
					arrF.Add(cFile.strFile);
				}
			}
			if(boolSubDirs)
			{
				foreach(clsDirectory cDir in arrDirectory)
				{
					if(!cDir.GetArrFile(ref arrF, strExtension, true))
					{
						return false;
					}
				}
			}

			return true;
		}
		public bool checkSubDirectories()
		{
			bool boolfullSubDir = false;
			Array strDirectories;
			strDirectories = System.IO.Directory.GetDirectories(strDirectory);

			foreach(String str in strDirectories)
			{
				Array strFiles;
				strFiles = System.IO.Directory.GetFiles(str, "*.tif");
				//int temp = strFiles.Length;
				if (strFiles.Length >= 1)
					boolfullSubDir = true;
				
			}
			return boolfullSubDir;
		}
	}
}
