using System;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsBatchGroup.
	/// </summary>
	public class clsBatchGroup
	{
		public clsWorkflow cWorkflow;
		public String strBatchGroup;
		public int intStartStatus;
		public ArrayList arrFolderLike;
		public String strBatchOutPath;
		public String strFMCs;
		public String strImageLikeStr;
		public String strBatchDateMethod;
		public String strErrorMultipage;
		public String strSplitMultipage;

		public String strAddToBatchName;
		public String strAddToBatchNamePos;
		public String strMatchName;
		public String strBatchTemplate;
		public String strBatchType;

		public bool boolCheckImageCount;
		public bool boolIncludeCoverSheetImageCount; //1.4.12 IncludeCoverSheet option for the CheckImageCount functionality
		public bool boolCopyVersion;
		
		//1.4.12 for PickupDateMethod
		public bool boolDoPickupDate;
		public String strPickupDateMask;
		public int intPickupDateDir;
		public String strPickupDateOutFormat;
		public String strPickupDateFullPath;

		public clsBatchGroup()
		{
			cWorkflow = null;
			strFMCs = "";
			strBatchGroup = "";
			strBatchOutPath = "";
			strImageLikeStr = "";
			arrFolderLike = new ArrayList();
			strBatchDateMethod = "";
			strErrorMultipage="";
			strAddToBatchName = "";
			strAddToBatchNamePos = "";
			strMatchName = "";
			strBatchTemplate = "";
			strBatchType = "";
			boolCheckImageCount = false;
			boolCopyVersion = true;

			//1.4.12
			boolDoPickupDate = false;
			strPickupDateMask = "";
			intPickupDateDir = 0;
			strPickupDateOutFormat = "";
			strPickupDateFullPath = "";
		}

		public bool FolderMatchesGroup(String strFolder)
		{
			if(strFolder == "")
			{
				return false;
			}
			
			foreach(String str in arrFolderLike)
			{
				if(UtilString.LikeStr(strFolder.ToUpper(), str.ToUpper()))
				{
					return true;
				}
			}
			return false;
		}
		
		public String GetModBatchName(String strImageFolder)
		{
			return GetModBatchName(strImageFolder, "");
		}
		public String GetModBatchName(String strImageFolder, String strBatchDate)
		{
			//return a modified batch name WITHOUT the path in front and WITHOUT ".mdb"
			String strModBatchName = "";
			String strAdd = strAddToBatchName;
			if(strBatchDate == "")
			{
				strBatchDate = GetBatchDate(strImageFolder);
			}
			if(UtilString.LikeStr(strAdd.ToUpper(), "*BATCHDATEMMDDYY*"))
			{
				strBatchDate = strBatchDate.Substring(0, 2) + strBatchDate.Substring(3, 2) + strBatchDate.Substring(8, 2);
				strAdd = UtilString.Replace(strAdd, "BATCHDATEMMDDYY", strBatchDate);
			}
			
			if(strAddToBatchName.ToUpper() == "CUSTOMMETHOD")
			{
				//use a custom method
				if(strAddToBatchNamePos.ToUpper() == "HBISUPERBILL")
				{
					//folder=T:\HBI\Images\Superbill\HBISuperbill_001
					//batchname=T:\HBI\Data\Superbill\HBI001.Mdb
					//strip out the word 'superbill_' in the middle
					strModBatchName = UtilFile.FileFromDir(UtilFile.DirNoSlash(strImageFolder));
					strModBatchName = strModBatchName.ToUpper();
					strModBatchName = UtilString.Replace(strModBatchName, "SUPERBILL_");
				}else if(strAddToBatchName.ToUpper() == "CCHBACKFILE")
				{
					//folder = T:\CONTRACOSTA\BACKFILE\122805\Box 1\021704\4048O_O_07
					//batch name: T:\Contracosta\Data\BACKFILE\021004_4041O_O_03.Mdb
					strModBatchName = UtilFile.DirNoSlash(strImageFolder);
					strAdd = UtilFile.FileFromDir(strModBatchName);
					strModBatchName = UtilFile.ContainingDirectory(strModBatchName);
					strModBatchName = UtilFile.DirNoSlash(strModBatchName);
					strModBatchName = UtilFile.FileFromDir(strModBatchName) + "_" + strAdd;
				}
				else
				{
					strModBatchName = "";
				}
			}
			else if(strAdd != "")
			{
				strModBatchName = UtilFile.FileFromDir(strImageFolder);
				if(UtilString.IsNumber(strAddToBatchNamePos))
				{
					strModBatchName = UtilString.Mid(strModBatchName, 1, Convert.ToInt32(
						strAddToBatchNamePos)) + strAdd +
						UtilString.Mid(strModBatchName, Convert.ToInt32(
						strAddToBatchNamePos) + 1);
				}
				else if(strAddToBatchNamePos.ToUpper() == "BEGINNING")
				{
					strModBatchName = strAdd + strModBatchName;
				}
				else if(strAddToBatchNamePos.ToUpper() == "END")
				{
					strModBatchName = strModBatchName + strAdd;
				}
				else
				{
					strModBatchName = "";
				}
			}
			return strModBatchName;
		}

		public String GetBatchDate(String strImageFolder)
		{
			//return a date that looks like "mm/dd/yyyy"
			String strTemp = strImageFolder, strTemp2 = "";
			strTemp = UtilFile.DirNoSlash(strTemp);
			strTemp = UtilFile.FileFromDir(strTemp);
			int intYear, intDayOfYear;
			String strCent;
			DateTime bDate;

			bDate = System.DateTime.Today;
			intYear = bDate.Year / 100; //get the current century
			strCent = intYear.ToString();

			if(strCent.Length != 2)
			{
				return "";
			}

			if(strBatchDateMethod.ToUpper() == "JULIAN1")
			{
				if(strTemp.Length < 5)
				{
					return "";
				}
				strTemp = strTemp.Substring(0, 5);
				strTemp2 = strTemp.Substring(0, 2);
				strTemp = strTemp.Substring(2, 3);
				intYear = Convert.ToInt32(strCent + strTemp2);
				bDate = new DateTime(intYear, 1, 1);
				intDayOfYear = Convert.ToInt32(strTemp) - 1;
				bDate = bDate.AddDays(intDayOfYear);

				strTemp = bDate.ToShortDateString();
				strTemp = UtilString.DateFormat(strTemp, "MM/DD/CCYY");
				return strTemp;
			}
			else if(strBatchDateMethod.ToUpper() == "JULIAN2")
			{
				// this will be using a single digit for the year...
				if(strTemp.Length < 4)
				{
					return "";
				}
				strTemp = strTemp.Substring(0, 4);
				strTemp2 = strTemp.Substring(0, 1);
				strTemp = strTemp.Substring(1, 3);

				// get the 2nd digit for the year from today's decade
				string strDecade = UtilString.DateFormat(DateTime.Today.ToShortDateString(), "YY").Substring(0, 1);
				strTemp2 = strDecade + strTemp2;

				intYear = Convert.ToInt32(strCent + strTemp2);
				bDate = new DateTime(intYear, 1, 1);
				intDayOfYear = Convert.ToInt32(strTemp) - 1;
				bDate = bDate.AddDays(intDayOfYear);

				strTemp = bDate.ToShortDateString();
				strTemp = UtilString.DateFormat(strTemp, "MM/DD/CCYY");
				return strTemp;
			}
			else if(strBatchDateMethod.ToUpper() == "YYMMDD1")
			{
				if(strTemp.Length < 10)
				{
					return "";
				}
				strTemp = strTemp.Substring(3, 6);
				strTemp2 = strCent + strTemp.Substring(0, 2);
				strTemp2 = strTemp.Substring(4, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "YYMMDD2")
			{
				if(strTemp.Length < 8)
				{
					return "";
				}
				strTemp = strTemp.Substring(0, 6);
				strTemp2 = strCent + strTemp.Substring(0, 2);
				strTemp2 = strTemp.Substring(4, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "YYMMDD3")
			{
				strTemp = UtilFile.DirNoSlash(strImageFolder);
				strTemp = UtilFile.ContainingDirectory(strTemp);
				strTemp = UtilFile.FileFromDir(strTemp);
				if(!UtilString.LikeStr(strTemp, "######"))
				{
					return "";
				}
				strTemp2 = strCent + strTemp.Substring(0, 2);
				strTemp2 = strTemp.Substring(4, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "MMDDYY1")
			{
				if(strTemp.Length < 8)
				{
					return "";
				}
				strTemp = strTemp.Substring(0, 6);
				strTemp2 = strCent + strTemp.Substring(4, 2);
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(0, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "MMDDYY2")
			{
				strTemp = UtilFile.DirNoSlash(strImageFolder);
				strTemp = UtilFile.ContainingDirectory(strTemp);
				strTemp = UtilFile.FileFromDir(strTemp);
				if(!UtilString.LikeStr(strTemp, "######"))
				{
					return "";
				}
				strTemp2 = strCent + strTemp.Substring(4, 2);
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(0, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "YYMMDD4")
			{
				if(strTemp.Length < 8)
				{
					return "";
				}
				strTemp = strTemp.Substring(2, 6);
				strTemp2 = strCent + strTemp.Substring(0, 2);
				strTemp2 = strTemp.Substring(4, 2) + "/" + strTemp2;
				strTemp2 = strTemp.Substring(2, 2) + "/" + strTemp2;
				return strTemp2;
			}
			else if(strBatchDateMethod.ToUpper() == "CURRENT")
			{
				return UtilString.DateFormat(DateTime.Today.ToShortDateString(), "MM/DD/CCYY");
			}
			else // custom format (ex. CCYYMMDD)
			{
				strTemp = strTemp.Substring(0, strBatchDateMethod.Length);
				strTemp = UtilDate.DateGetNewFormat(strTemp, strBatchDateMethod, "MM/DD/CCYY");
				
                if(strTemp == "01/01/1900") // bad date
					return "";

				return strTemp;
			}
			//return "";
		}
	}
}
