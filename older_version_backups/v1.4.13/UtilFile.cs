using System;
using System.Collections;
using System.IO;
namespace DocustreamNS
{
	/// <summary>
	/// 
	/// </summary>
	
		public class UtilFile
		{
			public UtilFile()
			{
				// 
				// TODO: Add constructor logic here
				//
			}

			public static String AppPath()
			{
				return DirNoSlash(System.AppDomain.CurrentDomain.BaseDirectory);
			}

			public static String DocustreamINI()
			{
				String strDir;
				strDir = ContainingDirectory(System.Environment.SystemDirectory);
				strDir = DirWithSlash(strDir);
				strDir = strDir + "Docustream.ini";
				return strDir;
			}

			public static String ContainingDirectory(String strFile)
			{
				String strPath;
				int intSlash;

				strPath = strFile;
				if(strFile=="" || strFile =="\\")
				{
					return strFile;
				}
				intSlash = strFile.LastIndexOf("\\");

				if(strFile.IndexOf(".")<0)
				{
					//it's a directory
					if(intSlash==strFile.Length)
					{
						strPath = strFile.Substring(0, strFile.Length - 1);
					}
				}

				intSlash = strPath.LastIndexOf("\\");

				if(intSlash==0)
				{
					return strFile;
				}
				strPath = strFile.Substring(0, intSlash);
				return strPath;
			}

			public static String DirWithSlash(String strDir)
			{
				String strTemp;
				if(strDir=="")
				{
					return "";
				}
				strTemp = strDir.Substring(strDir.Length - 1, 1);
				if(strTemp!= "\\")
				{
					return strDir + "\\";
				}
				else
				{
					return strDir;
				}
			}

			public static String DirNoSlash(String strDir)
			{
				if((strDir.Substring(strDir.Length - 1, 1)!= "\\"))
				{
					return strDir;
				}

				if(strDir.Length == 1)
				{
					return "";
				}

				return strDir.Substring(0, strDir.Length -1);
			}

		public static long SplitFile(String strFilePath, ArrayList arrFile)
		{
			arrFile.Clear();

			if(!FileIsThere(strFilePath))
			{
				return -1;
			}
			System.IO.StreamReader sr = new System.IO.StreamReader(strFilePath);

			String strTemp;
			strTemp = sr.ReadLine();

			while(strTemp != null)
			{
				if(strTemp!="")
					arrFile.Add(strTemp);

				strTemp = sr.ReadLine();
			}
			sr.Close();
			sr = null;
			return arrFile.Count;
		}

		public static void WriteToFile(String strText, String strFile)
		{
			try
			{
				StreamWriter sr = new StreamWriter(strFile, true);
				sr.WriteLine (strText);
				sr.Close();
				sr = null;
			}
			catch
			{
				
			}
		}

		public static bool FileIsThere(String strFile)
		{
			return File.Exists(strFile);
		}
		
		public static String FileFromDir(String strFile)
		{
			String strPath;
			int intSlash;

			if(!UtilString.LikeStr(strFile, "*\\*"))
			{
				return strFile;
			}

			if(strFile == "")
			{
				return "";
			}

			intSlash = UtilString.InStrRev(strFile, "\\");
			if(intSlash == 0)
			{
				return "";
			}

			strPath = UtilString.Mid(strFile, intSlash + 1);

			return strPath;
		}

		public static String NameMinusExtension(String strFile)
		{
			int intPos;
			String retVal;
			retVal = "";
			intPos = UtilString.InStrRev(strFile, ".");
			if(intPos <= 1)
			{
				return retVal;
			}

			retVal = UtilString.Mid(strFile, 1, intPos - 1);
			return retVal;
		}

		public static bool DeleteDir(String strdirectory)
		{
			Directory.Delete(strdirectory, true);
			return Directory.Exists(strdirectory) ? true : false;
		}
		
		public static void KillFile(String strFile)
		{
			try
			{
				File.Delete(strFile);
			}
			catch
			{
				
			}
		}
		
		public static bool DirIsThere(String strdirectory)
		{
			return Directory.Exists(strdirectory);
		}
		
		public static bool MakePath(String strdirectory)
		{
			if(!DirIsThere(strdirectory))
			{
				Directory.CreateDirectory(strdirectory);
				return DirIsThere(strdirectory) ? true : false;
			}
			return true;
		}

		public static bool IsDirectory(String strFile)
		{
			return ((File.GetAttributes(strFile) & FileAttributes.Directory) == FileAttributes.Directory);
		}
		
		public static bool OpenFileCopy(String strFileFrom, String strFileTo)
		{
			return OpenFileCopy(strFileFrom, strFileTo, true);
		}
		public static bool OpenFileCopy(String strFileFrom, String strFileTo, bool boolOverWrite)
		{
			try
			{
				File.Copy(strFileFrom, strFileTo, boolOverWrite);
				return true;
			}
			catch
			{
				return false;
			}
		}
		public static Boolean PrintIDXFile (String strIDXFile)
		{
			// 1.4.13 updates 
			// changed the return type of this function from void to Boolean
			// - Since this function creates the BatchLoader.idx file just in time when writing two output lines to the file,
			//   if the file is present right after writing to the file, return true or
			//   if the file is not present right after writing to the file, reutrn false
			//   The Boolean return value should indicate if the problem creating the BatchLoader.idx file occurs at the time of file creation or not.

			//pre-condition: the BatchLoader.idx file is not present in the batch image folder before the file creation (If it is, the batch should have been errorred earlier in the process)
			//post-condition: the BatchLoader.idx file is expected to be present in the batch image folder

			String strIDXOutput1;
			String strIDXOutput2;
			String strIDXPath;
			int intAttempt = 1;
			
			strIDXOutput1 ="[DocuLoader]\t" ; 
			strIDXOutput2 ="StartIndex=1\t";

			strIDXPath = strIDXFile + "\\BatchLoader.idx";

			while (intAttempt <= 2)  //1.4.13 give 2 retries to create BatchLoader.idx 
			{
				UtilFile.WriteToFile(strIDXOutput1, strIDXPath);
				UtilFile.WriteToFile(strIDXOutput2, strIDXPath);
				if (UtilFile.FileIsThere(strIDXPath)) 
				{
					return true;
				} 
				intAttempt++;
			}
			return false;
		}

	}
}
