using System;
using System.Collections;
using System.IO;

namespace DocustreamNS
{
	/// <summary>
	/// 
	/// </summary>

	public class clsFile
	{
		public ArrayList arrLine;
		public String strFile;

		public clsFile(String strfile) : this(strfile, false)
		{
			// 
			// TODO: Add constructor logic here
		}

		public clsFile(String strfile, bool boolPopulate)
		{
			// 
			// TODO: Add constructor logic here
			
			arrLine = new ArrayList();
			strFile = strfile;
			if(boolPopulate)
			{
				Populate();
			}
		}

		public bool Populate()
		{
			if(UtilFile.FileIsThere(strFile))
			{
				UtilFile.SplitFile(strFile, arrLine);
				return true;
			}
			else
			{
				return false;
			}
		}

		public long NumLines()
		{
			return arrLine.Count;
		}
	}
}
