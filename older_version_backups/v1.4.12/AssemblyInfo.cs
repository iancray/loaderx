using System.Reflection;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("DocuLoaderX")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.4.12")]
//version 1.1.6 - yymmdd2 option
//version 1.1.7 - a couple of bug fixes: 1. not outputting error on initialize 2. wasn't loading
//				  batch groups correctly
//version 1.1.8 - option to error out if multipage images are found
//version 1.1.9 - option to split multipage .tif images like the normal doculoader does
//version 1.2.0 - yymmdd3 option
//version 1.2.5 - added a bunch of stuff so i jumped it up 5 versions... :
//				  multiple batch group option (load folder into 1 or more wf's at once)
//				  and addToBatchName option
//1.2.6 - CURRENT date option for batch date
//1.2.7 - MMDDYY2 batch date method
//1.2.8 - changing "InsertFMCFromWorkflow" - was locking the batch, had to change some code
//				 to not use the "clsADONet" class for updating
//1.2.9 - there was still something in the background locking the batch ... so after closing
//			it and setting to null (at the end of create batch), force the garbage collection:
//			system.gc.collect();
//1.3.0 - added some custom batch name formatting for use with the option "addtoBatchname"..
//        i should've called the option 'modifybatchname'
//1.3.1 - messagebox at the end if any folders couldn't be loaded
//1.3.2 - YYMMDD4 batch date method
//1.3.3 - boolCheckImageCount option
//1.3.4 - 041108 - Added new option MatchName that rejects batches if the filename does not match the image folder name
//					If the batch group is not specified, it affects all batchgroups (global)
//					If there is a global setting plus a batchgroup setting, the batchgroup setting takes precedence
//					Use 1 to switch on (it is off by default)
//					ex.
//					ProgramName	BatchGroup	Option		Parameter
//					DocuLoaderX	CSLICODENT	MatchName	1	
//				 - Modified BatchTemplate option to allow use of a different batch template than the global one
//					This is differentiated from global one by specifying the batchgroup
//					ex.
//					ProgramName	BatchGroup	Option			Parameter	
//					DocuLoaderX	CSLICODENT	BatchTemplate	T:\batchtemplate2.mdb
//				 - Added ability to also set batch template in the GUI
//					This takes precedence over any settings in the master db	
//1.3.5 - 091208 - Now copies TRC version from workflow in batches - does this by default, but can be turned off with
//				    CopyVersion 0 in val/master database
//1.3.6 - 091708 - Added in creation of BatchLoader.idx file - was accidentally ommitted?
//1.3.7 - 120808 - When splitting multipage tifs - pads with 0's - starting from 3 digits, up to 6 digits if necessary
//1.3.8 - 120808 - Added NumericComparer.cs and StringLogicalComparer.cs from http://www.codeproject.com/KB/recipes/csnsort.aspx
//				   This does a string numeric sort (without converting to numbers) so that we get:
//					07092500853_1.tif
//					07092500853_2.tif
//					07092500853_10.tif
//				   instead of:
//					07092500853_1.tif
//					07092500853_10.tif
//					07092500853_2.tif
//1.4.0 - 051809 - support for loading batches into SQL workflows
//1.4.1 - 071009 - fixed bug where workflow settings were not always properly saved in a batch group if there was a SQL workflow
//1.4.2 - 100109 - when loading into SQL workflow, it was not excluding zones marked to be excluded. this is because access uses -1 for true
//				   and 0 for false, while SQL server uses 1 for true and 0 for false. fixed to only load zones where ExcludeZone = 0
//1.4.3 - 110509 - added Julian2 in clsBatchGroup.GetBatchDate() to support 4 digit julian dates (single digit for the year)
//1.4.4 - 111209 - added command line options
//				 - exitcode is set to 1 when there is an error
//1.4.5 - 022411 - added support for custom date formats in clsBatchGroup.GetBatchDate() (ex. CCYYMMDD)
//1.4.6 - 032911 - added batch group option 'BatchType' -- will fill the BatchType column in the workflow with this value if present
//1.4.7 - 040411 - allows the BatchType option to be specified on the command line
//1.4.8 - 042111 - fixed custom date format in clsBatchGroup.GetBatchDate() - trims end of batch folder name before attempting to convert it to a date
//1.4.9 - ?      - Unknown
//1.4.10- 112513 - If folder has sub folders with tif files the program will throw an error message
//1.4.11- 112917 - added a dated subfolder option for BatchOutPath 
//1.4.12- 012918 - Set up PickupDateMethod for recoding scan/pickup dates for the batches
//      - 020118 - added IncludeCoverSheet option to the CheckImageCount functionality

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
