using System;
using System.Collections;
using System.Windows.Forms;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsDocuProgram.
	/// </summary>
	public class clsDocuProgram
	{
		public static Form1 frmMain;
		public static String strLogFile;
		public static String strProgramName;
		public static clsOptions cOptions;
		public static bool boolStop;
		public static bool boolRunning;

		public static bool boolUseDefaultBatchTemplate = true;
		public static string strCustomBatchTemplate = "";

		public clsDocuProgram()
		{
			
		}

		public bool Init(String strProgName, Form1 formMain, String strMasterDB, String strCmdLineLog)
		{
			frmMain = formMain;
			strProgramName = strProgName;

			// set the log file
			String strDate = DateTime.Today.ToShortDateString();
			strDate = UtilString.DateFormat(strDate, "YYMMDD");
			if(strCmdLineLog != "")
				strLogFile = strCmdLineLog;
			else
				strLogFile = UtilFile.DirWithSlash(UtilFile.AppPath()) + strProgName + strDate + ".log"; //cOptions.strLogFilePath + strProgName + strDate + ".log";

			// initialize options
			cOptions = new clsOptions();
			String strErr = cOptions.Initialize(strProgName, strMasterDB);
			if(strErr != "")
			{
				if(frmMain.boolSuppressMessages)
				{
					Log(strErr);
					System.Environment.ExitCode = 1; // to notify other programs that automatically call LoaderX that there was an error
				}
				else
					MessageBox.Show(strErr);
				return false;
			}

			boolStop = false;
			boolRunning = false;
			return true;
		}
		
		public bool Start()
		{
			String strStatus;

			//this is the main file or directory to be processed
			if(boolRunning)
			{
				return false;
			}

			boolRunning = true;
			GUICmdState("START", "DISABLED");
			boolStop = false;

			GUIText("STATUS", "Running...");
			ArrayList arrFolder = frmMain.GetArrFolder();
			if(arrFolder.Count == 0)
			{
				GUIText("STATUS", "No batches selected");
				boolRunning = false;
				return true;
			}
			GUIText("STATUS", "Initializing...");
			clsLoaderProcess cProcess = new clsLoaderProcess();
			strStatus = cProcess.Init(cOptions);
			if(strStatus != "")
			{
				Log(strStatus);
				GUIText("STATUS", strStatus);
				cProcess = null;
				boolRunning = false;
				return false;
			}

			String strBatchDateManual = "";

			//do initial pass over batches to tell whether we need the user to specify
			// a manual batch date
			for(int i=arrFolder.Count - 1; i>=0; i--)
			{
				if(cProcess.RequireManualBatchDate(arrFolder[i].ToString()))
				{
					if(frmMain.dtBatchDate.Enabled)	{
						strBatchDateManual = frmMain.dtBatchDate.Text;
					}
					else{
						if(frmMain.boolSuppressMessages)
						{
							Log("1 or more batches require a manual batch " +
								"date to be specified. Please select one before proceeding.");
						}
						else
						{
							MessageBox.Show("1 or more batches require a manual batch " +
								"date to be specified. Please select one before proceeding.", 
								"", System.Windows.Forms.MessageBoxButtons.OK);
						}

						cProcess.DisconnectWorkflows();	
						cProcess = null;
						frmMain.dtBatchDate.Enabled = true;
						GUIText("STATUS", "Select a batch date");
						boolStop = false;
						boolRunning = false;
						return false;
					}
				}
			}

			GUIProgBar("MIN", 0);
			GUIProgBar("MAX", arrFolder.Count);
			GUIText("STATUS", "Loading...");
			for(int i=arrFolder.Count - 1; i>=0; i--)
			{
				GUIProgBar("VALUE", arrFolder.Count - i);
				if(boolStop)
				{	strStatus = "STOPPED";  break;	}

				strStatus = cProcess.ProcessFolder(arrFolder[i].ToString(), strBatchDateManual);

				if(strStatus == "")
				{
					UtilFile.PrintIDXFile(arrFolder[i].ToString());
					frmMain.ListBoxRemove(arrFolder[i].ToString());
				}else
				{
					//error
					Log(strStatus);
				}

				Application.DoEvents();
			}

			strStatus = "FINISHED";
			cProcess.DisconnectWorkflows();
			cProcess = null;

			GUIProgBar("VALUE", 0);

			if (strStatus == "FINISHED")
			{
				GUICmdState("START", "ENABLED");
				GUIText("STATUS", "Done");
				if(frmMain.lstFolderPaths.Items.Count > 0)
				{
					if(!frmMain.boolSuppressMessages)
					{
						MessageBox.Show("One or more folders couldn't be loaded.  "
							+ "Check to make sure they have correct batch names, and are located in the correct directory."
							+ "\n\nSee log for more details.");
					}
					GUIText("STATUS", "Error - One or more folders couldn't be loaded. See log for details.");  // 1.4.12 There was an error, so the Status on the GUI should inform the user.
					System.Environment.ExitCode = 1; // to notify other programs that automatically call LoaderX that there was an error
				}
			}
			else if (strStatus == "STOPPED")
			{
				GUIText("STATUS", "Stopped");
				GUICmdState("START", "ENABLED");
				GUIProgBar("VALUE", GUIProgBar("MIN", 0, true));
			}
			else if(strStatus == "ERROR")
			{
				GUIText("STATUS", "Error");
				System.Environment.ExitCode = 1; // to notify other programs that automatically call LoaderX that there was an error
			}
			else
			{
				//we should never encounter this
				if(frmMain.boolSuppressMessages)
					Log("FATAL ERROR!");
				else
					MessageBox.Show("FATAL ERROR!");
			}
			
			frmMain.dtBatchDate.Enabled = false;

			boolRunning = false;
			return true;
		}

		public void StopProgram()
		{
			boolStop = true;
			if(!boolRunning)
			{
				//If(the program already is running); this will be
				// taken care of after the batches/workflows are closed, etc
				GUIText("STATUS", "Stopped");
				GUICmdState("START", "ENABLED");
			}

			if(GUIProgBar("MIN", 0, true) != GUIProgBar("MAX", 0, true))
			{
				GUIProgBar("VALUE", GUIProgBar("MIN", 0, true));
			}
		}

		public static void Log(String strWrite)
		{
			UtilFile.WriteToFile(strWrite, strLogFile);
		}

		public static int GUIProgBar(String strAction, int intValue)
		{
			return GUIProgBar(strAction, intValue, false);
		}

		public static void GUICmdState(String strCmd, String strAction)
		{
			System.Windows.Forms.Button btnPtr;
			if(strCmd == "START")
			{
				btnPtr = frmMain.btnStart;
			}
		}

		public static void GUIText(String strTextBox, String strText)
		{
			switch(strTextBox.ToUpper())
			{	
				case "STATUS" :
					frmMain.txtStatus.Text = strText;
					break;
				case "MASTER" :
					frmMain.txtMaster.Text = strText;
					break;
				default :
					break;
			}
			Application.DoEvents();
		}

		public static void DoEvents()
		{
			Application.DoEvents();
		}

		public static int GUIProgBar(String strAction, int intValue, bool boolGet)
		{
			//this function is for easy access to the prog bar.. uses a static variable..
			// now we can interact with the progress bar much easier (like it's not limited by 32000)
			try
			{
				switch(strAction.ToUpper())
				{
					case "MIN" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Minimum;
						}
							frmMain.ProgressBar1.Minimum = intValue;
							break;
					case "MAX" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Maximum;
						}
							frmMain.ProgressBar1.Maximum = intValue;
							break;
					case "VALUE" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Value;
						}
							frmMain.ProgressBar1.Value = intValue;
							break;
					default:
						break;
				}
			}
			catch
			{
				//error... just keep going
			}
			return -1;
		}
	}
}
