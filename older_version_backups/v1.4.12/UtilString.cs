using System;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.Win32;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for UtilString.
	/// </summary>
	public class UtilString
	{
		public UtilString()
		{
		}

		public static String CharListPunct()
		{
			return ".,;'\"!_-@#$%^&*()+=\\/|?><`~[]{}: \r\n�";
		}

		public static String CharListInt(bool IncludeDec, bool IncludeNeg)
		{
			string strRet= "01232456789";
			if(IncludeDec)
			{
				strRet += ".";
			}
			if(IncludeNeg)
			{
				strRet += "-";
			}
			
			return strRet;
		}

		public static String DelimFieldFromEnd(String str, char strDelim, int intField)
		{
			string []arrTemp = str.Split(strDelim);
			String strRet;
			if(arrTemp.Length > 0)
			{
				if(intField >= 1 && intField <= arrTemp.Length)
				{
					strRet = arrTemp[arrTemp.Length - intField];
				}
				else
				{
					strRet = "";
				}
				return strRet;
			}
			else
			{
				return "";
			}
		}

		public static String FillChars(String strToAppendTo, String strChar, int intTotalLength, bool boolFront)
		{
			while( strToAppendTo.Length < intTotalLength)
			{
				if(boolFront)
				{
					strToAppendTo = strChar + strToAppendTo;
				}
				else
				{
					//'append to back
					strToAppendTo = strToAppendTo + strChar;
				}
			}

			return strToAppendTo;
		}

		public static void Run(String strCommand, String strArguments)
		{
			//System.Diagnostics.Process prcNew = new System.Diagnostics.Process();
			
			System.Diagnostics.Process.Start(strCommand, strArguments);
			//if(boolWaitFor)
			//{
			//	prcNew.WaitForExit();
			//}
		}
		
		public static ArrayList Split(String str, String split)
		{
			//return an array of strings
			ArrayList arrRet = new ArrayList();
			arrRet.Clear();
	
			String strTemp;
			int i, intLen;
			int intTemp1;
			intLen = str.Length; 
			int intLenSplit;
			intLenSplit = split.Length;

			if(intLen==0 || intLenSplit==0)
			{
				return arrRet;
			}

			int intCount = Count(str, split);
			if(intCount==0)
			{
				arrRet.Add(str);
				return arrRet;
			}

			i = 0;
			while(i <= intLen)
			{
				intTemp1 = str.IndexOf(split, i);
				if(intTemp1<0)
				{
					strTemp = str.Substring(i, intLen - i);
					arrRet.Add(strTemp);
					return arrRet;
				}
				else
				{
					strTemp = str.Substring(i, intTemp1 - i); 
					arrRet.Add(strTemp);
					i = intTemp1 + intLenSplit;
				}
			}
			return arrRet;
		}

		public static bool InArr(ArrayList arr, String strCheck)
		{
			for(int i=0; i<arr.Count; i++)
			{
				if(arr[i].ToString().ToUpper() == strCheck.ToUpper())
				{
					return true;
				}
			}
			return false;
		}

		public static bool IsNumber(String strCheck)
		{
			int intTemp;
			try
			{
				intTemp = Convert.ToInt32(strCheck);
				return true;
			}
			catch
			{
				return false;
			}
		}
		
		public static void SaveSetting(String strProgram, String strSetting, String strValue)
		{
			RegistryKey MyReg;
			try
			{
				MyReg = Registry.CurrentUser.CreateSubKey
					("SOFTWARE\\NETProgs\\" + strProgram);
				
				MyReg.SetValue(strSetting, strValue);		
				MyReg.Close();
			}
			catch
			{
				
			}
		}

		public static String GetSetting(String strProgram, String strSetting)
		{
			String strRet;
			try
			{
				RegistryKey MyReg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\NETProgs\\" + strProgram);
				
				strRet = (String)MyReg.GetValue(strSetting, "");
				
				MyReg.Close();
				return strRet;
			}
			catch
			{
				return "";
			}
		}
						
		public static String DelimField(String str, String strDelim, int intField)
		{
			String strTemp = "";
			int i;
			int intTemp1;

			int intLen = str.Length;
			int intLenDelim = strDelim.Length;

			if(intLen==0 || intLenDelim==0 || intField <=0)
			{
				return "";
			}

			int intCount = Count(str, strDelim);
			if(intCount==0 && intField==1)
			{
				return str;
			}

			if(intField > intCount + 1)
			{
				return "";
			}

			int intCurrPos=1;

			i = 1;
			while(intCurrPos <= intField)
			{
				intTemp1 = InStr(i, str, strDelim);
				if(intTemp1<=0)	
				{
					if(intCurrPos==intField)
					{
						strTemp= Mid(str, i, intLen - i + 1);
					}
					return strTemp;
				}
				else
				{

					strTemp= Mid(str, i, intTemp1 - i);

					if(intCurrPos==intField)
					{	
						return strTemp;
					}
					i = intTemp1 + intLenDelim;
					intCurrPos = intCurrPos + 1;
				}
			}

			return "";
		}

		public static String LikeStrToRegEx(String strLikeStr)
		{
			String strRet;
			strRet = Regex.Escape(strLikeStr); //replaces certain strings with their escape codes
			
			strRet = strRet.Replace("#", "[0-9]");		
			strRet = strRet.Replace("\\#", "[0-9]");
			strRet = strRet.Replace("\\[", "[");
			strRet = "^" + strRet.Replace("\\*", ".*").Replace("\\?", ".") + "$";
			return strRet;
		}

		public static bool LikeStr(String strToCheck, String strLike)
		{
			//regular expressions
			//isalpha ("[^a-zA-Z]");
			//isalphanumeric ("[^a-zA-Z0-9]");
			//isnumber ("[^0-9]");

			strLike = LikeStrToRegEx(strLike);
			Regex objPositivePattern = new Regex(strLike, RegexOptions.IgnoreCase);
			return objPositivePattern.IsMatch(strToCheck);
		}

		public static int InStrRev(String strValue, String strFind)
		{
			return strValue.LastIndexOf(strFind) + 1;
		}
		
		public static int InStr(int intStart, String strValue, String strFind)
		{
			return strValue.IndexOf(strFind, intStart - 1) + 1;
		}
		
		public static String UCase(String strValue)
		{
			return strValue.ToUpper();
		}

		public static String Trim(String strValue)
		{
			return strValue.Trim();
		}

		public static String Mid(String strValue, int intStart)
		{
			return Mid(strValue, intStart, -1);
		}

		public static String Mid(String strValue, int intStart, int intLen)
		{
			if(intStart < 1)
			{
				intStart = 1;
			}
			if(intStart > strValue.Length)
			{
				return "";
			}
			if(intLen==0)
			{
				return "";
			}else if(intStart - 1 + intLen > strValue.Length)
			{
				return strValue.Substring(intStart - 1);
			}else if(intLen > 0)
			{
				return strValue.Substring(intStart - 1, intLen);
			}else
			{	//intLen < 0.. return the whole rest of the string
				return strValue.Substring(intStart - 1);
			}
		}

		public static int Len(String strValue)
		{	
			return strValue.Length;
		}

		public static String ReplacePunct(String str, String strReplaceWith)
		{
			String strPunctList = CharListPunct();
			str = Replace(str, strPunctList, strReplaceWith);
			return str;
		}

		public static String ReplaceNonInt(String str, String strReplaceWith, bool RemoveDec, bool RemoveNeg)
		{
			String strList = CharListInt(!RemoveDec, !RemoveNeg);
			str = Replace(str, strList, strReplaceWith, true);
			return str;
		}

		public static String Replace(String strValue, String strReplace)
		{
			return Replace(strValue, strReplace, "");
		}
		public static String Replace(String strValue, String strReplace, String strReplaceWith)
		{
			return strValue.Replace(strReplace, strReplaceWith);
		}

		public static String Replace(String str, String strCharList, String strReplaceWith, bool boolInverse)
		{
			//for just a regular replace function (not a char list, but full strings - use str.Replace)

			//strReplaceChars is a list of characters to replace ie "1234567890" .. would emulate a "replaceint" function
			//                     (unless boolInverse was set to true, then it would emulate a "replacenonint" function
			//strReplaceWith : a string to replace each character in the character list with (this can be any length)
			//boolInverse: if set to false, all characters in the charlist will be replaced, otherwise, everything else
			// will be replaced
			
			int i, c;
			if(str.Length == 0)
			{
				return "";
			}
			
			c = str.Length;
			for(i=c-1; i>=0; i--)
			{
				if(boolInverse)
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) < 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
				else
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) >= 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
			}

			return str;
		}

		public static String ReplaceByIndex(String str, String strReplaceWith, int intStart, int intEnd)
		{
			// intStart and intEnd are inclusive
			string strRet = "";

			if(intStart > 0)
				strRet = str.Substring(0, intStart);

			strRet += strReplaceWith;
			strRet += str.Substring(intEnd+1);

			return strRet;
		}
		
		public static String ReplaceCred(String strValue, String strReplace)
		{
			String strVal;
			String strRep, strRet;
			
			if(strReplace != " ")
			{
				strRep = strReplace + " ";
			}
			else
			{
				strRep = " ";
			}
			strVal = " " + strValue.ToUpper().Trim() + " "; //add space on ends to assist with replaces

			//'comma ones must come first!

			strVal = Replace(Replace(Replace(strVal, ",ACSW ", strRep), ", ACSW ", strRep), " ACSW ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",ARNP ", strRep), ", ARNP ", strRep), " ARNP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",AST ", strRep), ", AST ", strRep), " AST ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",AST. ", strRep), ", AST. ", strRep), " AST. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",CNM ", strRep), ", CNM ", strRep), " CNM ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",CPO ", strRep), ", CPO ", strRep), " CPO ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",CP ", strRep), ", CP ", strRep), " CP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",CRNA ", strRep), ", CRNA ", strRep), " CRNA ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DC ", strRep), ", DC ", strRep), " DC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D.C. ", strRep), ", D.C. ", strRep), " D.C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",DC. ", strRep), ", DC. ", strRep), " DC. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D.C ", strRep), ", D.C ", strRep), " D.C ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D C. ", strRep), ", D C. ", strRep), " D C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D C ", strRep), ", D C ", strRep), " D C ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D. C. ", strRep), ", D. C. ", strRep), " D. C. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DDS ", strRep), ", DDS ", strRep), " DDS ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",DDS. ", strRep), ", DDS. ", strRep), " DDS. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D.D.S. ", strRep), ", D.D.S. ", strRep), " D.D.S. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D D S ", strRep), ", D D S ", strRep), " D D S ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D. D. S. ", strRep), ", D. D. S. ", strRep), " D. D. S. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DIR ", strRep), ", DIR ", strRep), " DIR ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",DIR. ", strRep), ", DIR. ", strRep), " DIR. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DMA ", strRep), ", DMA ", strRep), " DMA ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DMD ", strRep), ", DMD ", strRep), " DMD ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DO ", strRep), ", DO ", strRep), " DO ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D.O. ", strRep), ", D.O. ", strRep), " D.O. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",DO. ", strRep), ", DO. ", strRep), " DO. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D.O ", strRep), ", D.O ", strRep), " D.O ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D O. ", strRep), ", D O. ", strRep), " D O. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D O ", strRep), ", D O ", strRep), " D O ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",D. O. ", strRep), ", D. O. ", strRep), " D. O. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DPM ", strRep), ", DPM ", strRep), " DPM ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",DR ", strRep), ", DR ", strRep), " DR ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",DR. ", strRep), ", DR. ", strRep), " DR. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",EMBS ", strRep), ", EMBS ", strRep), " EMBS ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FAAP ", strRep), ", FAAP ", strRep), " FAAP ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F.A.A.P. ", strRep), ", F.A.A.P. ", strRep), " F.A.A.P. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",FAAP. ", strRep), ", FAAP. ", strRep), " FAAP. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F.A.A.P ", strRep), ", F.A.A.P ", strRep), " F.A.A.P ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F A A P ", strRep), ", F A A P ", strRep), " F A A P ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FP ", strRep), ", FP ", strRep), " FP ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",FP. ", strRep), ", FP. ", strRep), " FP. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F.P ", strRep), ", F.P ", strRep), " F.P ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F.P. ", strRep), ", F.P. ", strRep), " F.P. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F P. ", strRep), ", F P. ", strRep), " F P. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",F P ", strRep), ", F P ", strRep), " F P ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FAGD ", strRep), ", FAGD ", strRep), " FAGD ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FACC ", strRep), ", FACC ", strRep), " FACC ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FC ", strRep), ", FC ", strRep), " FC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",FC. ", strRep), ", FC. ", strRep), " FC. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",FNP ", strRep), ", FNP ", strRep), " FNP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",INC ", strRep), ", INC ", strRep), " INC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",INC. ", strRep), ", INC. ", strRep), " INC. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",INS ", strRep), ", INS ", strRep), " INS ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",INS. ", strRep), ", INS. ", strRep), " INS. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",JNC ", strRep), ", JNC ", strRep), " JNC ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",JR/PA ", strRep), ", JR/PA ", strRep), " JR/PA ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",LAC ", strRep), ", LAC ", strRep), " LAC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",LAC. ", strRep), ", LAC. ", strRep), " LAC. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",LCSW ", strRep), ", LCSW ", strRep), " LCSW", strRep);

			strVal = Replace(Replace(Replace(strVal, ",LTD ", strRep), ", LTD ", strRep), " LTD ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",LTD. ", strRep), ", LTD. ", strRep), " LTD. ", strRep);

			strVal = Replace(strVal, "MD ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",MD ", strRep), ", MD ", strRep), " MD ", strRep);
			strVal = Replace(Replace(Replace(strVal, " MD, ", strRep), " MD,", strRep), " MD, ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",M.D. ", strRep), ", M.D. ", strRep), " M.D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",MD. ", strRep), ", MD. ", strRep), " MD. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",M.D ", strRep), ", M.D ", strRep), " M.D ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",M D. ", strRep), ", M D. ", strRep), " M D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",M D ", strRep), ", M D ", strRep), " M D ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",M. D. ", strRep), ", M. D. ", strRep), " M. D. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",MN ", strRep), ", MN ", strRep), " MN ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",MRCP ", strRep), ", MRCP ", strRep), " MRCP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",MS ", strRep), ", MS ", strRep), " MS ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",MT ", strRep), ", MT ", strRep), " MT ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",NP ", strRep), ", NP ", strRep), " NP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",OD ", strRep), ", OD ", strRep), " OD ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O.D. ", strRep), ", O.D. ", strRep), " O.D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",OD. ", strRep), ", OD. ", strRep), " OD. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O.D ", strRep), ", O.D ", strRep), " O.D ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O D. ", strRep), ", O D. ", strRep), " O D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O. D. ", strRep), ", O. D. ", strRep), " O. D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O D ", strRep), ", O D ", strRep), " O D ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",OTC ", strRep), ", OTC ", strRep), " OTC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O.T.C. ", strRep), ", O.T.C. ", strRep), " O.T.C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",OTC. ", strRep), ", OTC. ", strRep), " OTC. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O. T. C. ", strRep), ", O. T. C. ", strRep), " O. T. C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",O T C ", strRep), ", O T C ", strRep), " O T C ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",OTCHT ", strRep), ", OTCHT ", strRep), " OTCHT ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",OTH ", strRep), ", OTH ", strRep), " OTH ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",OTR ", strRep), ", OTR ", strRep), " OTR ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PA ", strRep), ", PA ", strRep), " PA ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.A. ", strRep), ", P.A. ", strRep), " P.A. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PA. ", strRep), ", PA. ", strRep), " PA. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.A ", strRep), ", P.A ", strRep), " P.A ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P A. ", strRep), ", P A. ", strRep), " P A. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P. A. ", strRep), ", P. A. ", strRep), " P. A. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P A ", strRep), ", P A ", strRep), " P A ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PAC ", strRep), ", PAC ", strRep), " PAC ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PC ", strRep), ", PC ", strRep), " PC ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.C. ", strRep), ", P.C. ", strRep), " P.C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PC. ", strRep), ", PC. ", strRep), " PC. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.C ", strRep), ", P.C ", strRep), " P.C ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P C. ", strRep), ", P C. ", strRep), " P C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P. C. ", strRep), ", P. C. ", strRep), " P. C. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P C ", strRep), ", P C ", strRep), " P C ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PHD ", strRep), ", PHD ", strRep), " PHD ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PHD. ", strRep), ", PHD. ", strRep), " PHD. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH.D. ", strRep), ", PH.D. ", strRep), " PH.D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH.D ", strRep), ", PH.D ", strRep), " PH.D ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH D. ", strRep), ", PH D. ", strRep), " PH D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH. D ", strRep), ", PH. D ", strRep), " PH. D ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH. D. ", strRep), ", PH. D. ", strRep), " PH. D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PH D ", strRep), ", PH D ", strRep), " PH D ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PNP ", strRep), ", PNP ", strRep), " PNP ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PSC ", strRep), ", PSC ", strRep), " PSC ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PSYD ", strRep), ", PSYD ", strRep), " PSYD ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PSY. D. ", strRep), ", PSY. D. ", strRep), " PSY. D. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PSY.D. ", strRep), ", PSY.D. ", strRep), " PSY.D. ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",PT ", strRep), ", PT ", strRep), " PT ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.T. ", strRep), ", P.T. ", strRep), " P.T. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",PT. ", strRep), ", PT. ", strRep), " PT. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P.T ", strRep), ", P.T ", strRep), " P.T ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P T. ", strRep), ", P T. ", strRep), " P T. ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",P T ", strRep), ", P T ", strRep), " P T ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",RD ", strRep), ", RD ", strRep), " RD ", strRep);

			strVal = Replace(Replace(Replace(strVal, ",RNFA ", strRep), ", RNFA ", strRep), " RNFA ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",RNP ", strRep), ", RNP ", strRep), " RNP ", strRep);
			strVal = Replace(Replace(Replace(strVal, ",RN ", strRep), ", RN ", strRep), " RN ", strRep);

			strRet = strVal.Trim();
			return strRet;
		}

		/// <summary>
		/// Reformat any string.  strSource, strFormat must be the same length and strFormat must contain
		/// UNIQUE characters.  example: strformat:12345678 strsource:19001122 stroutformat:56/78/1234
		/// Output: 11/22/1900
		/// </summary>
		/// <param name="strSource"></param>
		/// <param name="strFormat"></param>
		/// <param name="strOutFormat"></param>
		/// <returns></returns>
		public static String StringFormat(String strSource, String strFormat, String strOutFormat)
		{
			String strRet = "";
			int intTemp;
			if(strFormat.Length != strSource.Length)
			{
				throw new Exception("String formatting parameters incorrect");
			}

			for(int i=0; i<strOutFormat.Length; i++)
			{
				intTemp = strFormat.IndexOf(strOutFormat[i]);
				if(intTemp < 0)
				{
					strRet += strOutFormat.Substring(i, 1);
				}
				else
				{
					strRet += strSource.Substring(intTemp, 1);
				}
			}
			return strRet;
		}

		public static String NameGrabLastName(String strName)
		{
			//'strname must look like this:
			//'jones, tom j
			//'or jones tom m

			int intPos;
			String strRet = "";
			strName = UtilString.Trim(strName);

			intPos = UtilString.InStr(1, strName, ",");
			if(intPos > 1)
			{
				strRet = UtilString.Trim(UtilString.Mid(strName, 1, intPos - 1));
			}
			else if(intPos == 0)
			{
				intPos = UtilString.InStr(1, strName, " ");

				if(intPos > 1)
				{
					strRet = UtilString.Trim(UtilString.Mid(strName, 1, intPos - 1));
				}
				else if(intPos == 0)
				{
					strRet = UtilString.Trim(strName);
				}
				else if(intPos == 1)
				{
					strRet = "";
				}
			}
			else if(intPos == 1)
			{
				strRet = "";
			}
			return strRet;
		}

		public static String NameGrabFirstName(String strName)
		{
			//'strname must look
			//'or jones tom m
			String strRet = "";
			int intPos;
			intPos = InStr(1, strName, ",");
			if(intPos == 0)
			{
				intPos = InStr(1, strName, " ");
				
				if(intPos == 0)
				{
					strRet = "";
				}
				else if(intPos <UtilString.Len(strName))
				{
					strRet = UtilString.Trim(UtilString.Mid(strName, intPos + 1));
				}
				else if(intPos == UtilString.Len(strName))
				{
					strRet = "";
				}
			}
			else if(intPos <UtilString.Len(strName))
			{
				strRet = UtilString.Trim(UtilString.Mid(strName, intPos + 1));
			}
			else if(intPos == UtilString.Len(strName))
			{
				strRet = "";
			}
			return strRet;
		}
		
		
		public static String NameMergeBeforeComma(String strName)
		{
			//'take spaces out of the name before the comma so last names dont get split up
			//' ie: MC DONNELL, CAROL   or  DE ANGELO, RIVERA

			int intFirst;
			int intFirstSpace;
			String retVal;

			intFirst = UtilString.InStr(1, strName, ",");
			intFirstSpace = UtilString.InStr(1, strName, " ");

			//'dont want to merge anything if the comma is the last character
			if((Len(strName) != intFirst) && (intFirst > 1))
			{
				if(intFirstSpace < intFirst && intFirstSpace < 4)
				{
					strName = UtilString.Replace(UtilString.Mid(strName, 1, intFirst - 1), " ", "") + UtilString.Mid(strName, intFirst);
				}
			}

			retVal = strName;
			return retVal;
		}
		
		public static String ReplaceExtraSpaces(String str)
		{
			while(str.IndexOf("  ", 0) > -1)
			{
				str = Replace(str, "  ", " ");
			}
			return str;
		}

		public static String NameSplit(String strName, String strDelim)
		{
			//'this function puts bars inbetween the last, first and middle names
			//' name should be in this format: LAST, FIRST MIDDLE
			//' or LAST FIRST MIDDLE
			//' Output looks like this: 'SMITH|JOHN|R.'

			int firstDelimPos;
			int secondDelimPos;
			int i;
			int length;
			String tempChar;
			String retVal;

			firstDelimPos = -1;
			secondDelimPos = -1;
			length = UtilString.Len(strName);

			//'this is here because names often come in like ' MC CALLAN, DONNA' and
			//' if we didn't call it, this function would output MC|CALLAN|DONNA
			strName = NameMergeBeforeComma(strName);

			strName = UtilString.Replace(strName, ",", " ");
			strName = ReplaceExtraSpaces(strName);

			for(i = 1; i <= length; i++)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					firstDelimPos = i;
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
					break;
				}
			}

			//'now reverse from the end
			for(i = length; i >=1; i--)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					secondDelimPos = i;
			        
					if(i <= firstDelimPos)
					{
						break;
					}
			        
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
			         
					//'if there is another space following, take it out out
					if(UtilString.Mid(strName, i + 1, 1) == " ")
					{
						strName = UtilString.Mid(strName, 1, i) + UtilString.Mid(strName, i + 2);
					}
					break;
				}
			}
			  
			//'there HAS to be 2 delimiters in this string at the end of this function
			while(Count(strName, strDelim) < 2)
			{
				strName = strName + strDelim;
			}

			retVal = strName;
			return retVal;
		}
		
		public static int Count(String strSource, String strCount)
		{
			int i;
			int c;
			int lngCount;
			c = 1;
			int retVal;
			if(strCount == "")
			{
				return 0;
			}
    
			lngCount = 0;
			i = UtilString.InStr(c, strSource, strCount);
    
			while(i != 0)
			{
				lngCount = lngCount + 1;
				c = i + UtilString.Len(strCount);
				i = UtilString.InStr(c, strSource, strCount);
			}	
    
			retVal = lngCount;
			return retVal;
		}

		public static bool DateIsPastDate(String strCurr, String strCheckIfPastOther)
		{
			DateTime dtCurr = DateGetDateTime(strCurr);
			DateTime dtCheckIfPastOther = DateGetDateTime(strCheckIfPastOther);
			return DateIsPastDate(dtCurr, dtCheckIfPastOther);
		}

		public static bool DateIsPastDate(DateTime dtCurr, DateTime dtCheckIfPastOther)
		{
			if(dtCurr.Equals(dtCheckIfPastOther))
			{
				return false;
			}

			TimeSpan dtTemp = dtCurr.Subtract(dtCheckIfPastOther);

			if(Convert.ToInt32(dtTemp.TotalDays) < 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static DateTime DateGetDateTime(String strDate)
		{
			//date must be in format: ##/##/####
			//RETURN a default date of 0 ticks on not valid
			String strTemp;
			int intYear, intMonth, intDay;
			DateTime dtDate = new DateTime(0);

			if(!LikeStr(strDate, "##/##/####"))
			{
				return dtDate;
			}

			strTemp = UtilString.DelimField(strDate, "/", 3);
			intYear = Convert.ToInt32(strTemp);
			strTemp = UtilString.DelimField(strDate, "/", 1);
			intMonth = Convert.ToInt32(strTemp);
			strTemp = UtilString.DelimField(strDate, "/", 2);
			intDay = Convert.ToInt32(strTemp);

			dtDate = new DateTime(intYear, intMonth, intDay);
			return dtDate;
		}
		
		public static int DateDiff(DateTime dtLater, DateTime dtEarlier)
		{
			TimeSpan dtTemp = dtLater.Subtract(dtEarlier);
			int intRet = Convert.ToInt32(dtTemp.TotalDays);
			return intRet;
		}

		public static String DateFormat(String strDate, String strFormat)
		{
			//strDate mustbe in format: MM/DD/YYYY
			// OR M/D/YYYY , etc
			//format looks something like this:
			//  MMDDCCYY OR YYMMDD OR CCYYMMDD or something like that :)
			String strCent = "", strMonth = "", strDay = "", strYear = "";
			
			strMonth = UtilString.DelimField(strDate, "/", 1);
			strDay = UtilString.DelimField(strDate, "/", 2);
			strYear = UtilString.DelimField(strDate, "/", 3);
			if(strMonth.Length == 1)
			{
				strMonth = "0" + strMonth;
			}
			if(strDay.Length == 1)
			{
				strDay = "0" + strDay;
			}
			strDate = strMonth + "/" + strDay + "/" + strYear;
			
			if(!UtilString.LikeStr(strDate, "??/??/????"))
			{
				return strDate;
			}
			
			
			strMonth = strDate.Substring(0, 2);
			strDay = strDate.Substring(3, 2);
			strCent = strDate.Substring(6, 2);
			strYear = strDate.Substring(8, 2);
			
			String strRet = strFormat.ToUpper();
			strRet = Replace(strRet, "MM", strMonth);
			strRet = Replace(strRet, "DD", strDay);
			strRet = Replace(strRet, "CC", strCent);
			strRet = Replace(strRet, "YY", strYear);
			return strRet;
		}
		
		public static String DateFromJulian(String strJulian)
		{
			//use current century
			String strCent;
			strCent = DateTime.Today.Year.ToString().Substring(0, 2);
			return DateFromJulian(strJulian, strCent);
		}

		public static String DateFromJulian(String strJulian, String strCent)
		{
			//5 digit date.. CCjjj
			// returns string date in the format: mm/dd/ccyy 
			// return blank on error
			DateTime bDate;
			String strTemp, strTemp2;
			int intYear, intDayOfYear;
			if(strJulian.Length != 5 || strCent.Length != 2)
			{
				return "";
			}

			strTemp = strJulian;
			strTemp2 = strTemp.Substring(0, 2);
			strTemp = strTemp.Substring(2, 3);
			intYear = Convert.ToInt32(strCent + strTemp2);
			bDate = new DateTime(intYear, 1, 1);
			intDayOfYear = Convert.ToInt32(strTemp) - 1;
			bDate = bDate.AddDays(intDayOfYear);

			strTemp = bDate.ToShortDateString();
			strTemp = UtilString.DateFormat(strTemp, "MM/DD/CCYY");
			return strTemp;
		}
		
	}
}
