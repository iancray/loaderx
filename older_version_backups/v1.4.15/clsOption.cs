using System;
using System.Collections;
namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsOption.
	/// </summary>
	public class clsOption
	{
		public String strDescription;
		public String strOption;
		public String strOtherParameters;
		public String strParameter;
		public String strProgramName;
		public String strBatchGroup;

		public clsOption()
		{
			// TODO: Add constructor logic here
			strDescription = "";
			strOption = "";
			strOtherParameters = "";
			strParameter = "";
			strProgramName = "";
			strBatchGroup = "";
		}


	}
}
