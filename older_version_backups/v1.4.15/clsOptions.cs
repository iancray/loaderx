using System;
using System.Collections;
using System.Data.OleDb;
using System.Data;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsOptions.
	/// </summary>
	public class clsOptions
	{
		//static member variable containing current instance
		//ini options
//		public String strWorkflow;
//		public int intStartBatchStatus;
		public String strValMan;
//		public String strLogFilePath;
//		public String strTemplate;

		//other options
		public ArrayList arrOption;

		public clsOptions()
		{
			// TODO: Add constructor logic here
			arrOption = new ArrayList();
//			strWorkflow = "";
			strValMan = "";
//			strLogFilePath = "";
		}

		public String Initialize(String strProgramName)
		{
			return Initialize(strProgramName, "");
		}
	
		public String Initialize(String strProgramName, String strValMan1)
		{
			String strRet = "";
			strValMan = strValMan1;
			OleDbConnection cnMan;
			if(strValMan1 == "")
			{
				return "Please specify a master database";
			}
			if(clsADONET.OleDBOpen(out cnMan, strValMan))
			{	strRet = ValManGetOptions(cnMan, strProgramName);	}
			else
			{	strRet = "ERROR: Couldn't open master database: " + strValMan;	}
		
			return strRet;
		}

//		public String InitializeINI(String strProgramName)
//		{
//			String strTemp;
//			String strINI = UtilFile.DocustreamINI();
//			clsFile cFile = new clsFile(strINI, true);
//			ArrayList arrLine = cFile.arrLine;
//
//			if(!ModuleIsThere(arrLine, strProgramName))
//			{	return "ERROR: " + strProgramName + " module doesn't exist in .ini";	}
//
//			strTemplate = INIGetOption(arrLine, "Docustream", "TemplatePath");
//			if(strTemplate != "")
//			{	strTemplate = UtilFile.DirWithSlash(strTemplate);	}
//
//			strTemplate = strTemplate + INIGetOption(arrLine, strProgramName, "TemplateDBName");
//			if(!UtilFile.FileIsThere(strTemplate))
//			{
//				if(strTemplate == "")
//				{
//					return "Batch template not specified";
//				}
//				else
//				{
//					return "Batch template not found";
//				}
//			}
//
//			strWorkflow = INIGetOption(arrLine, "Docustream", "WorkflowDatabase");
//			strTemp = INIGetOption(arrLine, strProgramName, "StartBatchStatusValue");
//			if(UtilString.IsNumber(strTemp))
//			{	intStartBatchStatus = Convert.ToInt32(strTemp);	}
//			else
//			{	return "ERROR: Invalid StartBatchStatusValue";	}
//
//			strLogFilePath = INIGetOption(arrLine, strProgramName, "LogFilePath");
//			return "";
//		}

		public String ValManGetOptions(OleDbConnection cnConn, String strProgramName)
		{
			String SQL;
			clsOption cOptTemp;
			bool boolOrder = false;

			if(!clsADONET.TableIsThere(cnConn, "ProgramOptions"))
			{
				return "";
			}

			if(clsADONET.ColumnIsThere(cnConn, "ProgramOptions", "Order"))
			{
				boolOrder = true;
			}
			SQL = "[ProgramOptions].[ProgramName]='" + strProgramName + "'";
			if(boolOrder)
			{
			  SQL = SQL + " ORDER BY [ProgramOptions].[Order]";
			}
			SQL = clsADONET.GetSQL("ProgramOptions", SQL);
			OleDbCommand cmd = new OleDbCommand(SQL, cnConn);
			OleDbDataReader rd = cmd.ExecuteReader();

			if(!rd.HasRows)
			{
				return "";
			}

			while(rd.Read())
			{
				cOptTemp = new clsOption();
				cOptTemp.strProgramName = strProgramName;
				if(rd["Parameter"] != DBNull.Value)
				{
					cOptTemp.strParameter = rd["Parameter"].ToString();
				}
				if(rd["OtherParameters"] != DBNull.Value)
				{
					cOptTemp.strOtherParameters = rd["OtherParameters"].ToString();
				}
				if(rd["Option"] != DBNull.Value)
				{
					cOptTemp.strOption = rd["Option"].ToString();
				}else{ continue; }
				if(rd["Description"] != DBNull.Value)
				{
					cOptTemp.strDescription = rd["Description"].ToString();
				}
				if(rd["BatchGroup"] != DBNull.Value)
				{
					cOptTemp.strBatchGroup = rd["BatchGroup"].ToString();
				}
				arrOption.Add(cOptTemp);
			}
			rd.Close();
			return "";
		}

		public String INIGetOption(ArrayList arrLine, String strModule, String strInfo)
		{
			bool boolInMod = false;
			int intInStr;
			
			foreach(String str in arrLine)
			{
				if("[" + strModule.ToUpper() + "]" == str.ToUpper())
				{
					boolInMod = true;
				}
				else
				{
					if(UtilString.LikeStr(str, "[*"))
					{
						if(boolInMod)
							break;
					}
					else
					{
						if(boolInMod)
						{
							if(UtilString.LikeStr(str.ToUpper(), strInfo.ToUpper() + "=*"))
							{
								intInStr = str.IndexOf("=");
								return str.Substring(intInStr + 1);
							}
						}
					}
				}
			}

			return "";
		}

		public bool ModuleIsThere(ArrayList arrLine, String strModule)
		{
			foreach(String str in arrLine)
			{
				if("[" + strModule.ToUpper() + "]" == str.ToUpper())
				{
					return true;
				}
			}
			return false;
		}

		public void AddOption(clsOption cOption)
		{
			arrOption.Add(cOption);
		}

		public clsOption GetProgramOptionsByIndex(int lngIndex)
		{
			if(lngIndex >= 1 && lngIndex <= arrOption.Count)
			{
				return (clsOption)arrOption[lngIndex];
			}

			return (clsOption)Convert.DBNull;
		}

		public bool ContainsOption(String strOption)
		{
			foreach(clsOption cOption in arrOption)
			{
				if(cOption.strOption.ToUpper() == strOption.ToUpper())
				{
					return true;
				}
			}

			return false;
		}

		public String GetOptionParameter(String strOption)
		{
			return GetOptionParameter(strOption, false, "");
		}
		public String GetOptionParameter(String strOption, bool boolOtherParams)
		{
			return GetOptionParameter(strOption, boolOtherParams, "");
		}
		public String GetOptionParameter(String strOption, bool boolOtherParams, String strBatchGroup)
		{
			foreach(clsOption cOption in arrOption)
			{
//				if(strBatchGroup != "")
//				{
					if(cOption.strBatchGroup.ToUpper().Trim() != strBatchGroup.ToUpper().Trim())
					{
						continue;
					}
//				}
				if(cOption.strOption.ToUpper() == strOption.ToUpper())
				{
					if(boolOtherParams)
					{
						return cOption.strOtherParameters;
					}
					else
					{
						return cOption.strParameter;
					}
				}
			}
			return "";
		}
		
		public clsOption GetOption(String strOption)
		{
			return GetOption(strOption, "");
		}
		public clsOption GetOption(String strOption, String strBatchGroup)
		{
			foreach(clsOption cOption in arrOption)
			{
				if(cOption.strOption.ToUpper() == strOption.ToUpper())
				{
					return cOption;
				}
			}
			return null;
		}

		public ArrayList GetArrOptions(String strOption)
		{
			return GetArrOptions(strOption, "");
		}
		public ArrayList GetArrOptions(String strOption, String strBatchGroup)
		{
			ArrayList arrRet = new ArrayList();
			foreach(clsOption cOption in arrOption)
			{
				if(strBatchGroup != "")
				{
					if(cOption.strBatchGroup.ToUpper() != strBatchGroup.ToUpper())
					{
						continue;
					}
				}
				if(cOption.strOption.ToUpper() == strOption.ToUpper())
				{
					arrRet.Add(cOption);
				}
			}
			return arrRet;
		}
		public String PrepPickupDateMethod(String strParameter, clsBatchGroup cThisBatchGroup, clsOptions cThisOptions)
		{
			// 1.4.12 populates settings needed for the PickupDateMethod functionality
			String strErr = "";
			ArrayList arrDir;
			int intStart, intEnd, intDir;
			arrDir = UtilString.Split(strParameter, "\\");
			intDir = 0;
			foreach(String strDir in arrDir)
			{
				intDir++;
				intStart = UtilString.InStr(1, strDir, "[");
				if (intStart > 0)
				{
					intEnd = UtilString.InStr(1, strDir, "]");
					if ((intEnd > 0) && (intEnd > (intStart + 1)))
					{
						cThisBatchGroup.strPickupDateMask = UtilString.Trim(UtilString.Mid(strDir, intStart + 1, intEnd - (intStart + 1)));
						cThisBatchGroup.intPickupDateDir = intDir;
						cThisBatchGroup.strPickupDateOutFormat = UtilString.Trim(cThisOptions.GetOptionParameter("PickupDateMethod", true, cThisBatchGroup.strBatchGroup));
						break;
					}
				}
			}
			if (cThisBatchGroup.strPickupDateMask == "" || cThisBatchGroup.intPickupDateDir < 1)
			{
				strErr = "ERROR: PickupDateMethod not set up correctly. Unable to determine a date subfolder mask from the full path specified for "
					     + cThisBatchGroup.strBatchGroup;
			}
			else if (cThisBatchGroup.strPickupDateOutFormat == "")
			{
				strErr = "ERROR: PickupDateMethod not set up correctly. Pickup date output format not specified for " + cThisBatchGroup.strBatchGroup;
			}
			else
			{
				cThisBatchGroup.strPickupDateFullPath = strParameter;
				cThisBatchGroup.boolDoPickupDate = true;
			}
			return strErr;
		}
	}
}
