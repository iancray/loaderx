using System;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsADONET.
	/// </summary>
	using System.Data;
	using System.Data.OleDb;

	public class clsADONET : IDisposable
	{

// ******************** SAMPLE CODE USE *****************************
// ******************** SAMPLE CODE USE *****************************
// ******************** SAMPLE CODE USE *****************************
//	clsADONET clADO = new clsADONET(cnVacationLog);
//	if(!clADO.FillDataTable("SELECT * FROM Employees;"))
//	{
//		MessageBox.Show(clADO.strErrorMsg);
//		return ;
//	}
//	foreach(DataRow dr in clADO.dTbl.Rows)
//	{
//		strTemp = dr["Last"].ToString() + ", " + dr["First"].ToString() + "                                                 " + Convert.ToInt32(dr["ID"]).ToString();
//		cmbEmployee.Items.Add(strTemp);
//	}		
//	clADO.Dispose();

		public OleDbConnection cnConn;
		OleDbDataAdapter myDataAdapter;
		OleDbCommandBuilder cBuilder;
		DataSet dSet;
		public DataTable dTbl;
		bool boolInitialized;
		public String strErrorMsg;

		public clsADONET()
		{
			//
			// TODO: Add constructor logic here
			boolInitialized = false;
			strErrorMsg = "";
		}

		public clsADONET(ref OleDbConnection cnConn)
		{	
			this.cnConn = cnConn;
			boolInitialized = true;
		}

		public clsADONET(ref OleDbConnection cnConn, String SQL)
		{	
			this.cnConn = cnConn;
			boolInitialized = true;
			FillDataTable(SQL);
		}

		public clsADONET(String strDB) : this(strDB, true)
		{		}

		public clsADONET(String strDB, bool boolConnect)
		{
			//
			// TODO: Add constructor logic here
			boolInitialized = false;
			strErrorMsg = "";
			if(boolConnect)
			{	
				OpenConn(strDB);
			}
		}

		public bool OpenConn(String strDB)
		{
			if(!boolInitialized)
			{
				if(OleDBOpen(out cnConn, strDB))
				{
					boolInitialized = true;
				}
				else
				{
					strErrorMsg = "Unable to open " + strDB;
				}
				return boolInitialized;
			}
			else
			{
				strErrorMsg = "Connection already open";
				return false;
			}
		}

		public static bool OleDBOpen(out OleDbConnection cnDB, String strDB)
		{
			return OleDBOpen(out cnDB, strDB, "", "", "");
		}
		public static bool OleDBOpen(out OleDbConnection cnDB, String strDB, string strUserID, string strPassword, string strSQLServer)
		{
			try
			{
				if(strSQLServer != "")
				{
					cnDB = new OleDbConnection("Provider=SQLOLEDB.1;Persist Security Info=True;Data Source=" + strSQLServer 
						+ ";Initial Catalog=" + strDB + ";User Id=" + strUserID + ";Password=" + strPassword + ";");
				}
				else
				{
					cnDB = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDB + ";" );
				}
				
				cnDB.Open();
				return true;
			}
			catch
			{
				cnDB = null;
				return false;
			}
		}

		public static bool TableIsThere(OleDbConnection cnConn, String strTable)
		{
			OleDbCommand cmd = null;
			try
			{
				cmd = new OleDbCommand("SELECT * FROM [" + strTable + "];", cnConn);
				OleDbDataReader rd = cmd.ExecuteReader();
				rd.Close();
				cmd.Dispose();
				return true;
			}
			catch
			{
				if(cmd != null)
				{
					cmd.Dispose();
				}
				return false;
			}		
		}

		public bool FillDataTable(String strSQL)
		{
			//sample use once you have the datatable
			//			foreach( DataRow dr in dt.Rows )
			//			{
			//				//if( dr["ID"] )
			//				dt.Rows.Remove(dr);
			//				//dt.Rows[ 0 ][ "City" ] = "Yes"; // or true if this is a boolean field
			//			}

			if(!boolInitialized)
			{
				strErrorMsg = "Connection not initialized";
				return false;
			}
			try
			{
				myDataAdapter = new OleDbDataAdapter();
				myDataAdapter.SelectCommand = new OleDbCommand(strSQL, cnConn);

				cBuilder = new OleDbCommandBuilder(myDataAdapter);

				//these are required if you have a field name that is
				// a reserved word.. like 'First'
				cBuilder.QuotePrefix = "[";
				cBuilder.QuoteSuffix = "]";

				dSet = new DataSet();
				myDataAdapter.Fill(dSet);
				dTbl = dSet.Tables[0];
				return true;
			}
			catch(Exception ex)
			{
				strErrorMsg = ex.Message;
				return false;
			}
		}

		public bool FillDataTableNoPrimaryKey(String strSQL, OleDbCommand updateCmd)
		{
			//sample use once you have the datatable
			//			foreach( DataRow dr in dt.Rows )
			//			{
			//				//if( dr["ID"] )
			//				dt.Rows.Remove(dr);
			//				//dt.Rows[ 0 ][ "City" ] = "Yes"; // or true if this is a boolean field
			//			}
			
			if(!boolInitialized)
			{
				strErrorMsg = "Connection not initialized";
				return false;
			}
			try
			{
				myDataAdapter = new OleDbDataAdapter();
				myDataAdapter.SelectCommand = new OleDbCommand(strSQL, cnConn);

				cBuilder = new OleDbCommandBuilder(myDataAdapter);
				myDataAdapter.UpdateCommand = updateCmd;

				//these are required if you have a field name that is
				// a reserved word.. like 'First'
				cBuilder.QuotePrefix = "[";
				cBuilder.QuoteSuffix = "]";

				dSet = new DataSet();
				myDataAdapter.Fill(dSet);
				dTbl = dSet.Tables[0];
				return true;
			}
			catch(Exception ex)
			{
				strErrorMsg = ex.Message;
				return false;
			}
		}

		//public bool Update(String strTable)
		public bool Update()
		{
			//String strTemp;
			try
			{
				//strTemp = myDataAdapter.UpdateCommand.CommandText;
				//Console.WriteLine(strTemp);
				myDataAdapter.Update(dTbl);
				return true;
			}catch(Exception ex)
			{ 
				strErrorMsg = "Unable to update data : " + ex.Message;
				return false; 
			}
		}

//		public void SetPrimaryKey(ArrayList arrColNames)
//		{
//			//used for arrays that don't 
//			DataColumn[] arrDataColumn;
//			
//			arrDataColumn = new DataColumn[arrColNames.Count];
//			//ArrayList dcDataColumn = new ArrayList();
//			int intIndex=0;
//			foreach(String str in arrColNames)
//			{
//				arrDataColumn[intIndex] = new DataColumn(str);
//				arrDataColumn[intIndex].DataType = System.Type.GetType("System.Int32");
//				//dcDataColumn.Add(new DataColumn(str));
//				intIndex++;
//			}
//
//			//dTbl.PrimaryKey = arrDataColumn;
//			//dTbl.PrimaryKey = new DataColumn[1];
//			DataColumn colTemp = new DataColumn("UserDefined_FieldID", System.Type.GetType("System.Int32"));
//			DataColumn[] keys = new DataColumn[1];
//			dTbl.PrimaryKey = keys;
//			//new DataColumn[] {workTable.Columns["CustLName"], 
//			//							workTable.Columns["CustFName"]};
//		}

		public static OleDbCommand BatchUserDefined_FieldIDGetUpdateCommand(OleDbConnection cnConnection)
		{
			//			// Create the SelectCommand.
			//			cmd = new OleDbCommand("SELECT * FROM Customers " +
			//				"WHERE Country = @Country AND City = @City", cnConn);
			//
			//			cmd.Parameters.Add("@Country", OleDbType.VarChar, 15);
			//			cmd.Parameters.Add("@City", OleDbType.VarChar, 15);
			//
			//			da.SelectCommand = cmd;
			// Create the UpdateCommand.

//it appears that the commandbuilder is unable to interpret and create the updatecommand based 
//on the selectcommand which is why i never use the commandbuilder

//with adap
//   .updatecommand = new sqlcommand
//   with .updatecommand
//      .connection = conn
//      .commandtext = "UPDATE mtch_ap_hdr SET MAH_STR_NUM = @MAH_STR_NUM, MAH_LAWSON_VEND = @MAH_LAWSON_VEND, ... WHERE MAH_ID = @MAH_ID"
//      with .parameters
//         .add("@MAH_ID", sqldbtype.int, 4, "MAH_ID")
//         .add("@MAH_STR_NUM", sqldbtype.int, 4, "MAH_STR_NUM") 'guessing on datatype
//         .add("@MAH_LAWSON_VEND, sqldbtype.varchar, 50, "MAH_LAWSON_VEND") 'guessing on size
//         ...
//      end with
//   end with
//end with

			try
			{
				OleDbDataAdapter da = new OleDbDataAdapter();
				OleDbCommand cmd;
				OleDbParameter parm;
				String SQL;

				SQL = "UPDATE [UserField_Value] SET [UserDefined_FieldID] = @UserDefined_FieldID, [Line_No] = @Line_No, " +
					" [Field_Value] = @Field_Value, [Image_ID] = @Image_ID " + 
					"WHERE [UserDefined_FieldID] = @oldUserDefined_FieldID AND [Line_No]=@oldLine_No AND [Image_ID]=@oldImage_ID;";

				SQL = "UPDATE [UserField_Value] SET " +
					" [Field_Value] = @Field_Value " + 
					"WHERE [UserDefined_FieldID] = @oldUserDefined_FieldID AND [Line_No]=@oldLine_No AND [Image_ID]=@oldImage_ID;";

//				SQL = "UPDATE [UserField_Value] SET [UserDefined_FieldID] = ?, [Line_No] = ?, " +
//					" [Field_Value] = ?, [Image_ID] = ? " + 
//					"WHERE [UserDefined_FieldID] = ? AND [Line_No]=? AND [Image_ID]=?";
//
//				SQL = "UPDATE UserField_Value SET UserDefined_FieldID = ?, Line_No = ?, " +
//					" Field_Value = ?, Image_ID = ? " + 
//					"WHERE UserDefined_FieldID = ? AND Line_No=? AND Image_ID=?";

				//SQL = "UPDATE [UserField_Value] SET [UserField_Value].[Line_No] = ? " +
				//	"WHERE [UserField_Value].[UserDefined_FieldID] = ? ";

				//			cmd = new OleDbCommand("UPDATE Customers SET CustomerID = @CustomerID, CompanyName = @CompanyName " +
				//				"WHERE CustomerID = @oldCustomerID", cnConn);
				cmd = new OleDbCommand(SQL, cnConnection);

				//these make it possible to update using a command builder
//				cmd.Parameters.Add("@UserDefined_FieldID", OleDbType.Char, 5, "UserDefined_FieldID");
//				cmd.Parameters.Add("@Line_No", OleDbType.Char , 5, "Line_No");
//				cmd.Parameters.Add("@Image_ID", OleDbType.Char , 5, "Image_ID");
				cmd.Parameters.Add("@Field_Value", OleDbType.VarChar, 40, "Field_Value");

				//cmd.Parameters.Add("@CompanyName", OleDbType.VarChar, 40, "CompanyName");
				parm = cmd.Parameters.Add("@oldUserDefined_FieldID", OleDbType.Char, 5, "UserDefined_FieldID");
				parm.SourceVersion = DataRowVersion.Original;
				cmd.Parameters.Add("@oldLine_No", OleDbType.Numeric, 5, "Line_No");
				parm.SourceVersion = DataRowVersion.Original;
				parm = cmd.Parameters.Add("@oldImage_ID", OleDbType.Char, 5, "Image_ID");
				parm.SourceVersion = DataRowVersion.Original;

				//parm = cmd.Parameters.Add("@oldCustomerID", OleDbType.Char, 5, "CustomerID");
				//parm.SourceVersion = DataRowVersion.Original;

				//da.UpdateCommand = cmd;

				//return da;
				return cmd;
			}
			catch
			{
				return null;
			}
		}

		public int Execute(String SQL)
		{
			return Execute(cnConn, SQL);
		}

		public static int Execute(OleDbConnection cnConnection, String SQL)
		{
			int intRet;
			OleDbCommand cmd = new OleDbCommand(SQL, cnConnection);
			intRet = cmd.ExecuteNonQuery();
			cmd.Dispose();
			return intRet;
		}

		public void Dispose()
		{
			Dispose(false);
		}

		public static void fullyCloseDB(ref OleDbConnection cnConnection)
		{
//			try
//			{
				if(cnConnection !=null)
				{
					if(cnConnection.State == System.Data.ConnectionState.Open)
					{
						cnConnection.Close();
					}
					cnConnection.Dispose();
					cnConnection = null;
				}
//			}
//			catch
//			{
//				
//			}
		}

		public void Dispose(bool boolCloseConn)
		{
//			if(!boolInitialized)
//			{	return ;	}
//			try
//			{
				if(!(myDataAdapter == null))
				{
					myDataAdapter.Dispose();
				}
				myDataAdapter = null;
				if(!(cBuilder == null))
				{
					cBuilder.Dispose();
				}
				cBuilder = null;
				if(!(dTbl == null))
				{
					dTbl.Dispose();
				}
				dTbl = null;
				if(boolCloseConn)
				{
					DisposeConn();
				}
//			}
//			finally
//			{
//			
//			}
		}
		
		public static bool FieldIsThere(OleDbDataReader rd, String strCol)
		{
			try
			{
				int intColOrd = rd.GetOrdinal(strCol);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool ColumnIsThere(OleDbConnection cnConn, String strTable, String strColumn)
		{
			String SQL;
			SQL = "";
			SQL = SQL + " SELECT [" + strTable + "].[" + strColumn + "]";
			SQL = SQL + " FROM [" + strTable + "];";
			
			try
			{
				OleDbCommand cmd = new OleDbCommand(SQL, cnConn);
				OleDbDataReader rd = cmd.ExecuteReader();
				cmd.Dispose();
				rd.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public void DisposeConn()
		{
			cnConn.Close();
			cnConn.Dispose();
			cnConn = null;
			boolInitialized = false;
		}

		public static String GetSQL(String strTable, String strCondition)
		{
			return GetSQL(strTable, strCondition, "", false);
		}
		public static String GetSQL(String strTable, String strCondition, String strOrderByColumn)
		{
			return GetSQL(strTable, strCondition, strOrderByColumn, false);
		}
		public static String GetSQL(String strTable)
		{
			return GetSQL(strTable, "", "", false);
		}
		public static String GetSQL(String strTable, String strCondition, String strOrderByColumn, bool boolDescending)
		{
			String SQL;
			SQL = "SELECT * FROM [" + strTable + "]";

			if(strCondition!="")
			{
				SQL += " WHERE " + strCondition;
			}

			if(strCondition!="")
			{
				if(strOrderByColumn != "")
				{
					SQL += " ORDER BY [" + strTable + "].[" + strOrderByColumn +"]";
					if(boolDescending)
					{
						SQL += " DESC";
					}
				}
			}

			SQL += ";";
			return SQL;
		}
		
		
		
		
		
		//		public bool Init(OleDbConnection cnConn, String strSQL, String strTable)
		//		{
		//			try
		//			{
		//				this.cnConn = cnConn;
		//				myDataAdapter = new OleDbDataAdapter( );
		//				myDataAdapter.SelectCommand = new OleDbCommand(strSQL, cnConn);
		//				cBuilder = new OleDbCommandBuilder(myDataAdapter);
		//				dSet = new DataSet();
		//
		//				myDataAdapter.Fill( dSet, strTable );
		//				dTbl = dSet.Tables[strTable];
		//				boolInitialized = true;
		//				return true;
		//			}catch{
		//				strErrorMsg = "Unable to initialize data";
		//				return false;
		//			}
		//		}

		//		public bool Init(String strDB)
		//		{
		//			if(OleDBOpen(out cnConn, strDB))
		//			{
		//				return true;
		//			}else{
		//				strErrorMsg = "Unable to open database: " + strDB;
		//				return false;
		//			}
		//		}
		//
		//		public bool Init(String strDB, String strSQL, String strTable)
		//		{
		//			if(!OleDBOpen(out cnConn, strDB))
		//			{	
		//				strErrorMsg = "Unable To Open Database";	
		//				return false;
		//			}
		//			else
		//			{
		//				return Init(this.cnConn, strSQL, strTable);
		//			}
		//		}


	}
}
