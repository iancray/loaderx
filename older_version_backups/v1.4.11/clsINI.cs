using System;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsINI.

	public class clsINI : clsFile 
	{
		public String strWorkflow;
		public String strLogFilePath;
		public int intStartStatus;
		public int intEndStatus;
		public int intBusyStatus;
		public int intErrorStatus;

		public clsINI(String strFile) : base(strFile)
		{
			// TODO: Add constructor logic here
		}

		public bool Init(String strProgramName)
		{
			strWorkflow = GetData("Docustream", "WorkflowDatabase");
			return true;
		}

		public String GetData(String strModule, String strInfo)
		{
			bool boolInMod = false;
			int intInStr;
			
			foreach(String str in arrLine)
			{
				if("[" + strModule.ToUpper() + "]" == str.ToUpper())
				{
					boolInMod = true;
				}else{
					if(UtilString.LikeStr(str, "[*"))
					{
						if(boolInMod)
							break;
					}else{
						if(boolInMod)
						{
							if(UtilString.LikeStr(str.ToUpper(), strInfo.ToUpper() + "=*"))
							{
								intInStr = str.IndexOf("=");
								return str.Substring(intInStr + 1);
							}
						}
					}
				}
			}

			return "";
		}

		/*
		public String GetData(ArrayList arrFile, String strModule, String strInfo)
		{
			bool boolHitModule;
			String strRet;
			strRet = "";
			boolHitModule = false;
			foreach (String o in arrFile)
			{
				if(o.ToUpper().StartsWith("[" + strModule.ToUpper()))
				{
					if(boolHitModule)
					{
						break;
					}
					boolHitModule = true;
				}

				if(boolHitModule)
				{
					if(o.ToUpper().StartsWith(strInfo.ToUpper() + "="))
					{
						strRet = o.Substring(strInfo.Length + 1);
						break;
					}
				}
			}
			return strRet;
		}
		*/
	}
}
