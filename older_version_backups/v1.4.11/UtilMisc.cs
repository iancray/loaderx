using System;
using System.Collections;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Reflection;

namespace DocustreamNS
{
	public delegate void LogFunction(string strMessage);
	public delegate void GUITextFunction(string strTextBox, string strText);
	public delegate int GUIProgBarFunction(string strAction, int intValue);
	public delegate int GUIProgBarFunctionPercent(int intPercent);
	
	public class UtilMisc
	{		
		public UtilMisc()
		{
		}
		
		public static string GetAssemblyName()
		{
			string strProgramName = "";

			Assembly assembly = Assembly.GetExecutingAssembly();
			object[] attributes = assembly.GetCustomAttributes(true);
			foreach (object attribute in attributes)
			{
				if (attribute is AssemblyTitleAttribute)
				{
					strProgramName = ((AssemblyTitleAttribute)attribute).Title;
					break;
				}
			}
			return strProgramName;
		}

		public static string GetAssemblyProduct()
		{
			return System.Windows.Forms.Application.ProductName;
		}

		public static string GetAssemblyVersion()
		{
			string strVersion = "";
			AssemblyName assemblyName = Assembly.GetExecutingAssembly().GetName();
			strVersion = assemblyName.Version.ToString();

			return strVersion;
		}

		public static string GetAssemblyVersionNoBuild()
		{
			string strVersion = GetAssemblyVersion();

			// remove build version (just use major, minor, and revision numbers)
			strVersion = strVersion.Substring(0, strVersion.LastIndexOf("."));

			return strVersion;
		}

		public static string BrowseFolder(string strStartDirOrFile)
		{
			return BrowseFolder(strStartDirOrFile, "");
		}

		public static string BrowseFolder(string strStartDirOrFile, string strDescription)
		{
			FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
			strStartDirOrFile = strStartDirOrFile.Trim();
			if (strStartDirOrFile != "")
			{
				if (UtilFile.DirIsThere(strStartDirOrFile))
				{
					folderBrowserDialog1.SelectedPath = strStartDirOrFile;
				}
				else if (UtilFile.DirIsThere(UtilFile.ContainingDirectory(strStartDirOrFile)))
				{
					folderBrowserDialog1.SelectedPath = UtilFile.ContainingDirectory(strStartDirOrFile);
				}
			}

			if(strDescription != "") 
				folderBrowserDialog1.Description = strDescription;

			DialogResult dlgRes = folderBrowserDialog1.ShowDialog();

			string strTemp = "";
			if(dlgRes == DialogResult.OK)
				strTemp = folderBrowserDialog1.SelectedPath;

			folderBrowserDialog1.Dispose();
			return strTemp;
		}

		public static string BrowseFile(string strStartDirOrFile, string strFilter)
		{
			return BrowseFile(strStartDirOrFile, strFilter, "");
		}

		public static string BrowseFile(string strStartDirOrFile, string strFilter, string strTitle)
		{
			OpenFileDialog openFileDialog1 = new OpenFileDialog();
			strStartDirOrFile = strStartDirOrFile.Trim();
			if (strStartDirOrFile != "")
			{
				if (UtilFile.DirIsThere(strStartDirOrFile))
				{
					openFileDialog1.InitialDirectory = strStartDirOrFile;
				}
				else if (UtilFile.DirIsThere(UtilFile.ContainingDirectory(strStartDirOrFile)))
				{
					openFileDialog1.InitialDirectory = UtilFile.ContainingDirectory(strStartDirOrFile);
				}
			}

			if(strTitle != "")
				openFileDialog1.Title = strTitle;

			openFileDialog1.Filter = strFilter;
			
			DialogResult dlgRes = openFileDialog1.ShowDialog();

			string strTemp = "";
			if(dlgRes == DialogResult.OK)
				strTemp = openFileDialog1.FileName;

			openFileDialog1.Dispose();
			return strTemp;
		}

		public static string BrowseSaveFile(string strStartDirOrFile, string strFilter, string strTitle, string strFilename)
		{
			SaveFileDialog saveFileDialog1 = new SaveFileDialog();
			strStartDirOrFile = strStartDirOrFile.Trim();
			if (strStartDirOrFile != "")
			{
				if (UtilFile.DirIsThere(strStartDirOrFile))
				{
					saveFileDialog1.InitialDirectory = strStartDirOrFile;
				}
				else if (UtilFile.DirIsThere(UtilFile.ContainingDirectory(strStartDirOrFile)))
				{
					saveFileDialog1.InitialDirectory = UtilFile.ContainingDirectory(strStartDirOrFile);
				}
			}

			if(strTitle != "")
				saveFileDialog1.Title = strTitle;

			saveFileDialog1.Filter = strFilter;
			saveFileDialog1.FileName = strFilename;
			
			DialogResult dlgRes = saveFileDialog1.ShowDialog();

			string strTemp = "";
			if(dlgRes == DialogResult.OK)
				strTemp = saveFileDialog1.FileName;

			saveFileDialog1.Dispose();
			return strTemp;
		}

		public static Object CreateObject(string strObjectName)
		{
			//Get the excel object 
			//ie "Excel.Application"
			Type objType = Type.GetTypeFromProgID(strObjectName);
			//Create instance of excel 
			Object myObject = Activator.CreateInstance(objType);
			return myObject;
		}

		/// <summary>
		/// Case insensitive version of String.IndexOf
		/// </summary>
		public static bool ArrInArr(ArrayList arr, String str)
		{
			foreach(String strAr in arr)
			{
				if(strAr.ToUpper() == str.ToUpper())
				{
					return true;
				}
			}
			return false;
		}
		
				
		public static String GetSetting(String strProgram, String strSetting)
		{
			String strRet;
			try
			{
				RegistryKey MyReg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\NETProgs\\" + strProgram);
				
				strRet = (String)MyReg.GetValue(strSetting, "");
				
				MyReg.Close();
				return strRet;
			}
			catch
			{
				return "";
			}
		}
		
		public static void GetSetting(String strProgram, System.Windows.Forms.Control cntrl)
		{
			switch(cntrl.GetType().ToString())
			{
				case "System.Windows.Forms.CheckBox" :
					if(GetSetting(strProgram, cntrl.Name) == "1")
					{
						((System.Windows.Forms.CheckBox)cntrl).Checked = true;
					}
					else
					{
						((System.Windows.Forms.CheckBox)cntrl).Checked = false;
					}
					break;
				case "System.Windows.Forms.RadioButton" :
					if(GetSetting(strProgram, cntrl.Name) == "1")
					{
						((System.Windows.Forms.RadioButton)cntrl).Checked = true;
					}
					else
					{
						((System.Windows.Forms.RadioButton)cntrl).Checked = false;
					}
					break;
				default :
					cntrl.Text = GetSetting(strProgram, cntrl.Name);
					break;
			}
		}
		
		public static bool InArr(ArrayList arr, String strCheck)
		{
			for(int i=0; i<arr.Count; i++)
			{
				if(arr[i].ToString().ToUpper() == strCheck.ToUpper())
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Safely set the value of a progress bar (in percent)
		/// </summary>
		public void GUIProgBar(System.Windows.Forms.ProgressBar ProgressBar1, int intPercent)
		{
			intPercent = (intPercent < 0 || intPercent > 100) ? 0 : intPercent;
			
			if(ProgressBar1.Minimum != 0 || ProgressBar1.Maximum != 100)
			{
				ProgressBar1.Minimum = 0;
				ProgressBar1.Maximum = 100;
			}
			ProgressBar1.Value = intPercent;
		}

		/// <summary>
		/// Safely set or get the value of a progress bar (no exceptions thrown)
		/// </summary>
		/// <param name="strAction">"MIN", "MAX" or "VALUE"</param>
		public int GUIProgBar(System.Windows.Forms.ProgressBar ProgressBar1, String strAction, int intValue, bool boolGet)
		{
			//this function is for easy access to the prog bar.. uses a static variable..
			// now we can interact with the progress bar much easier (like it's not limited by 32000)
			try
			{
				switch(strAction.ToUpper())
				{
					case "MIN" :
						if(boolGet)
						{
							return ProgressBar1.Minimum;
						}
						ProgressBar1.Minimum = intValue;
						break;
					case "MAX" :
						if(boolGet)
						{
							return ProgressBar1.Maximum;
						}
						ProgressBar1.Maximum = intValue;
						break;
					case "VALUE" :
						if(boolGet)
						{
							return ProgressBar1.Value;
						}
						ProgressBar1.Value = intValue;
						break;
					default:
						break;
				}
			}
			catch
			{
				//error... just keep going
			}
			return -1;
		}
	
		public static void SaveSetting(String strProgram, String strSetting, String strValue)
		{
			RegistryKey MyReg;
			try
			{
				MyReg = Registry.CurrentUser.CreateSubKey
					("SOFTWARE\\NETProgs\\" + strProgram);
				
				MyReg.SetValue(strSetting, strValue);		
				MyReg.Close();
			}
			catch
			{
				
			}
		}
		
		public static void SaveSetting(String strProgram, System.Windows.Forms.Control cntrl)
		{
			switch(cntrl.GetType().ToString())
			{
				case "System.Windows.Forms.CheckBox" :
					if(((System.Windows.Forms.CheckBox)cntrl).Checked)
					{
						SaveSetting(strProgram, cntrl.Name, "1");
					}
					else
					{
						SaveSetting(strProgram, cntrl.Name, "0");
					}
					break;
				case "System.Windows.Forms.RadioButton" :
					if(((System.Windows.Forms.RadioButton)cntrl).Checked)
					{
						SaveSetting(strProgram, cntrl.Name, "1");
					}
					else
					{
						SaveSetting(strProgram, cntrl.Name, "0");
					}
					break;
				default :
					SaveSetting(strProgram, cntrl.Name, cntrl.Text);
					break;
			}
		}
		
		public static bool CompareByteArrays (byte[] data1, byte[] data2)
		{
			// If both are null, they're equal
			if (data1==null && data2==null)
			{
				return true;
			}
			// If either but not both are null, they're not equal
			if (data1==null || data2==null)
			{
				return false;
			}
			if (data1.Length != data2.Length)
			{
				return false;
			}
			for (int i=0; i < data1.Length; i++)
			{
				if (data1[i] != data2[i])
				{
					return false;
				}
			}
			return true;
		}

	}
}
