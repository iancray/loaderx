using System;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsLoaderProcess.
	/// </summary>
	public class clsLoaderProcess
	{
		public clsBatchGroups cBatchGroups;
		public String strBatchTemplate;
		public ArrayList arrWorkflow;
		public bool boolMatchName; // global, use this if not specified in batchgroup
		public bool boolCopyVersion; // global, use this if not specified in batchgroup

		public clsLoaderProcess()
		{
			// TODO: Add constructor logic here
			cBatchGroups = null;
			strBatchTemplate = "";
			arrWorkflow = new ArrayList();
			boolMatchName = false;
			boolCopyVersion = true;
		}

		public bool RequireManualBatchDate(String strImageFolder)
		{
			ArrayList arrBatchGroup;
			arrBatchGroup = cBatchGroups.BatchGroupsFromImageFolder(strImageFolder);
			foreach(clsBatchGroup cBatchGroup in arrBatchGroup)
			{
				if(cBatchGroup.strBatchDateMethod.ToUpper() == "MANUAL")
					return true;
			}

			return false;
		}

		public String ProcessFolder(String strImageFolder)
		{
			return ProcessFolder(strImageFolder, "");
		}

		public String ProcessFolder(String strImageFolder, String strBatchDateManual)
		{
			String strErr = "";
			String strErrRet = "";
			ArrayList arrBatchGroups;
			arrBatchGroups = cBatchGroups.BatchGroupsFromImageFolder(strImageFolder);
			
			if(arrBatchGroups.Count == 0)
			{
				return "ERROR: No batch group found for folder: " + strImageFolder;
			}
			//1 image folder can correspond to multiple batch groups... 
			// so far this is only for aah where an image folder is used for 
			// js and the regular workflows as well
			foreach(clsBatchGroup cBatchGroup in arrBatchGroups)
			{
				strErr = CreateBatchFromGroup(strImageFolder, cBatchGroup, strBatchDateManual);
				if(strErr != "")
				{
					if(strErrRet != "")
					{	strErrRet += "\r";	}
					strErrRet += strErr + " Batch Group : " + cBatchGroup.strBatchGroup;
				}
			}
			return strErrRet;
		}

		private String CreateBatchFromGroup(String strImageFolder, clsBatchGroup cBatchGroup, 
											String strBatchDateManual)
		{
			String strErr = "";			
			String strBatchDate = "";
			String strModBatchName;
			clsBatch cBatch = null;
			if(cBatchGroup.strBatchDateMethod == "")
			{	
				return "ERROR: No batch date method specified";		
			}
			else if(cBatchGroup.strBatchDateMethod.ToUpper() == "MANUAL")
			{	
				strBatchDate = strBatchDateManual;
			}
			else
			{
				strBatchDate = cBatchGroup.GetBatchDate(strImageFolder);
			}
			if(strBatchDate == "")
			{
				return "ERROR: Unable to obtain batch date";
			}
			else if(!UtilString.LikeStr(strBatchDate, "##/##/####"))
			{
				return "ERROR: Invalid batch date : " + strBatchDate + " for folder: " + strImageFolder;
			}

			cBatch = new clsBatch("");
			if(!cBatchGroup.cWorkflow.boolConnected)
			{
				if(!cBatchGroup.cWorkflow.Connect())
				{
					strErr = "ERROR: Unable to open workflow : " + cBatchGroup.cWorkflow.strWorkflow;
				}
			}
			
			if(strErr == "")
			{
				string strTemplateToUse;
				bool boolCheckMatch;
				bool boolCopyVer = boolCopyVersion;

				strModBatchName = cBatchGroup.GetModBatchName(strImageFolder, strBatchDateManual);

				// select template (there are 3 possiblities: 1 custom from form, 2 from ProgramOptions
				if(clsDocuProgram.boolUseDefaultBatchTemplate && cBatchGroup.strBatchTemplate != "")
					strTemplateToUse = cBatchGroup.strBatchTemplate;
				else
					strTemplateToUse = strBatchTemplate;

				if(cBatchGroup.strMatchName == "")
					boolCheckMatch = boolMatchName;
				else
					boolCheckMatch = (cBatchGroup.strMatchName == "1");

				if(cBatchGroup.boolCopyVersion == false)
					boolCopyVer = false; // default is true so only need to override global if group is false

				strErr = cBatch.CreateBatch(strImageFolder, cBatchGroup.cWorkflow, 
					cBatchGroup.strBatchOutPath, cBatchGroup.strFMCs, 
					strTemplateToUse, cBatchGroup.strImageLikeStr, 
					cBatchGroup.strErrorMultipage == "1", 
					cBatchGroup.strSplitMultipage == "1", 
					strModBatchName, cBatchGroup.boolCheckImageCount, boolCheckMatch, boolCopyVer);
			}
			
			if(strErr == "")
			{	
				strErr = cBatchGroup.cWorkflow.AddBatch(cBatch, cBatchGroup.intStartStatus, strBatchDate, 50, cBatchGroup.strBatchType);
				if(strErr == "")
				{
					clsDocuProgram.Log("Loaded Batch : " + cBatch.strBatch + " into : " + cBatchGroup.cWorkflow.strWorkflow + " " + System.DateTime.Now.ToString());
				}
			}

			if(cBatch != null)
			{	
				clsADONET.fullyCloseDB(ref cBatch.cnBatch);	
			}

			cBatch = null;
			return strErr;
		}	

#region *****INITIALIZATION***
		public String Init(clsOptions cOptions)
		{
			//organize batch groups and their
			// parameters in memory for easy access
			String strErr;
			String strTemp;

			//STORE TEMPLATE
			if(clsDocuProgram.boolUseDefaultBatchTemplate)
				strBatchTemplate = cOptions.GetOptionParameter("BatchTemplate");
			else
				strBatchTemplate = clsDocuProgram.strCustomBatchTemplate;

			if(strBatchTemplate == "")
			{
				return "ERROR: No batch template specified";
			}
			else if(!UtilFile.FileIsThere(strBatchTemplate))
			{
				return "ERROR: Batch template not found : " + strBatchTemplate;
			}

			// global, only use this if not specified in batchgroup
			boolMatchName = (cOptions.GetOptionParameter("MatchName") == "1");
			boolCopyVersion = (cOptions.GetOptionParameter("CopyVersion") != "0"); // default is true

			//STORE WORKFLOWS
			InitWorkflows(cOptions);
			if(arrWorkflow.Count == 0)
			{
				return "ERROR: No workflows specified in master database";
			}

			//STORE BATCH GROUPS
			strErr = InitBatchGroups(cOptions);
			if(strErr != "")
			{
				return strErr;
			}

			//STORE THE REST OF THE PARAMETERS
			foreach(clsBatchGroup cBatchGroup in cBatchGroups.arrBatchGroup)
			{
				cBatchGroup.strFMCs = cOptions.GetOptionParameter("FMC", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strBatchOutPath = cOptions.GetOptionParameter("BatchOutPath", false, cBatchGroup.strBatchGroup);
				strTemp = cOptions.GetOptionParameter("StartBatchStatusValue", false, cBatchGroup.strBatchGroup);
				if(UtilString.IsNumber(strTemp))
				{
					cBatchGroup.intStartStatus = Convert.ToInt32(strTemp);
				}
				else
				{
					cBatchGroup.intStartStatus = 10;
				}
				cBatchGroup.strImageLikeStr = cOptions.GetOptionParameter("ImageLikeStr", false, cBatchGroup.strBatchGroup);				
				cBatchGroup.strBatchDateMethod = cOptions.GetOptionParameter("BatchDateMethod", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strErrorMultipage = cOptions.GetOptionParameter("ErrorMultipage", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strSplitMultipage = cOptions.GetOptionParameter("SplitMultipage", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strAddToBatchName = cOptions.GetOptionParameter("AddToBatchName", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strAddToBatchNamePos = cOptions.GetOptionParameter("AddToBatchName", true, cBatchGroup.strBatchGroup);
				cBatchGroup.boolCheckImageCount = (cOptions.GetOptionParameter("CheckImageCount", false, cBatchGroup.strBatchGroup) == "1");
				cBatchGroup.strMatchName = cOptions.GetOptionParameter("MatchName", false, cBatchGroup.strBatchGroup);
				cBatchGroup.strBatchTemplate = cOptions.GetOptionParameter("BatchTemplate", false, cBatchGroup.strBatchGroup);
				cBatchGroup.boolCopyVersion = (cOptions.GetOptionParameter("CopyVersion", false, cBatchGroup.strBatchGroup) != "0"); // default is true
				if(clsDocuProgram.frmMain.strCmdLineBatchType != "")
					cBatchGroup.strBatchType = clsDocuProgram.frmMain.strCmdLineBatchType;
				else
					cBatchGroup.strBatchType = cOptions.GetOptionParameter("BatchType", false, cBatchGroup.strBatchGroup);
			}

			return "";
		}

		public bool BatchGroupAssignWorkflow(clsOptions cOptions, clsBatchGroup cBatchGroup)
		{
			ArrayList arrTemp;

			bool boolSQLServer = false;
			string strUserID = "";
			string strPassword = "";
			string strSQLServer = "";

			// first see if there are settings for a SQL workflow for this batch group
			foreach(clsOption cOpt in cOptions.arrOption)
			{
				arrTemp = UtilString.Split(cOpt.strBatchGroup, "|");
				if(UtilString.InArr(arrTemp, cBatchGroup.strBatchGroup))
				{
					if(cOpt.strOption.ToUpper() == "SQLWORKFLOW")
					{
						if(cOpt.strParameter == "1")
							boolSQLServer = true;
						else
							break;
					}
					else if(cOpt.strOption.ToUpper() == "USERID")
						strUserID = cOpt.strParameter;
					else if(cOpt.strOption.ToUpper() == "PASSWORD")
						strPassword = cOpt.strParameter;
					else if(cOpt.strOption.ToUpper() == "SQLSERVER")
						strSQLServer = cOpt.strParameter;
				}
			}

			foreach(clsOption cOpt in cOptions.arrOption)
			{
				if(cOpt.strOption.ToUpper() == "WORKFLOWGROUP")
				{
					arrTemp = UtilString.Split(cOpt.strBatchGroup, "|");
					if(UtilString.InArr(arrTemp, cBatchGroup.strBatchGroup))
					{
						foreach(clsWorkflow cWF in arrWorkflow)
						{
							if(cWF.strWorkflow.ToUpper() == cOpt.strParameter.ToUpper())
							{
								cBatchGroup.cWorkflow = cWF;

								if(boolSQLServer)
								{
									cWF.boolSQLWF = true;
									cWF.strUserID = strUserID;
									cWF.strPassword = strPassword;
									cWF.strSQLServer = strSQLServer;
								}
								return true;
							}
						}
						//if we got here, there is no matching workflow for it
						break;
					}
				}
			}
			return false;
		}
		
		private void InitWorkflows(clsOptions cOptions)
		{
			arrWorkflow.Clear();
			clsWorkflow cWF;
			foreach(clsOption cOpt in cOptions.arrOption)
			{
				if(cOpt.strOption.ToUpper() == "WORKFLOWGROUP")
				{
					cWF = new clsWorkflow(cOpt.strParameter);
					arrWorkflow.Add(cWF);
				}
			}
		}

		private String InitBatchGroups(clsOptions cOptions)
		{
			cBatchGroups = new clsBatchGroups();
			clsBatchGroup cBG;
			int i;
			ArrayList arrTemp1, arrTemp2;
			foreach(clsOption cOpt in cOptions.arrOption)
			{
				if(cOpt.strOption.ToUpper() == "BATCHGROUPS")
				{
					//multiple batch groups to add specified in this 1 option
					arrTemp1 = UtilString.Split(cOpt.strParameter, "|");
					arrTemp2 = UtilString.Split(cOpt.strOtherParameters, "|");
					if(arrTemp1.Count != arrTemp2.Count)
					{
						return "ERROR: Batch group parameter set up incorrectly : " + cOpt.strParameter;
					}

					for(i=0; i<arrTemp1.Count; i++)
					{
						if(cBatchGroups.BatchGroupExists(arrTemp1[i].ToString()))
						{
							return "ERROR: Duplicate batch group found : " + arrTemp1[i].ToString();
						}
						cBG = new clsBatchGroup();
						cBG.strBatchGroup = arrTemp1[i].ToString();
						cBG.arrFolderLike.Add(arrTemp2[i].ToString());
						if(!BatchGroupAssignWorkflow(cOptions, cBG))
						{
							return "ERROR: No workflow found for batch group: " + cBG.strBatchGroup;
						}
						cBatchGroups.arrBatchGroup.Add(cBG);
					}
				}
				else if(cOpt.strOption.ToUpper() == "BATCHGROUPSINGLE")
				{
					//single batch group to add
					if(cBatchGroups.BatchGroupExists(cOpt.strParameter))
					{
						return "ERROR: Duplicate batch group found : " + cOpt.strParameter;
					}

					cBG = new clsBatchGroup();
					cBG.strBatchGroup = cOpt.strParameter;
					arrTemp1 = UtilString.Split(cOpt.strOtherParameters, "|");
					foreach(String str in arrTemp1)
					{
						cBG.arrFolderLike.Add(str);
					}
					if(!BatchGroupAssignWorkflow(cOptions, cBG))
					{
						return "ERROR: No workflow found for batch group: " + cBG.strBatchGroup;
					}
					cBatchGroups.arrBatchGroup.Add(cBG);
				}
				else if(cOpt.strOption.ToUpper() == "BATCHGROUPMULTIPLE")
				{
					//multiple batch groups to add
					arrTemp1 = UtilString.Split(cOpt.strParameter, "|");

					foreach(String strBatchGroup in arrTemp1)
					{
						if(cBatchGroups.BatchGroupExists(strBatchGroup))
						{
							return "ERROR: Duplicate batch group found : " + strBatchGroup;
						}

						cBG = new clsBatchGroup();
						cBG.strBatchGroup = strBatchGroup;
						arrTemp2 = UtilString.Split(cOpt.strOtherParameters, "|");
						foreach(String str in arrTemp2)
						{
							cBG.arrFolderLike.Add(str);
						}
						if(!BatchGroupAssignWorkflow(cOptions, cBG))
						{
							return "ERROR: No workflow found for batch group: " + cBG.strBatchGroup;
						}
						cBatchGroups.arrBatchGroup.Add(cBG);
					}
				}
			}

			if(cBatchGroups.arrBatchGroup.Count == 0)
			{
				return "ERROR: No batch groups found in options database";
			}
			return "";
		}
#endregion

		public void DisconnectWorkflows()
		{
			foreach(clsWorkflow cWF in arrWorkflow)
			{
				cWF.Disconnect();
			}
		}
		
		
	}
}
