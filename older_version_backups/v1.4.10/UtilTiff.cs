using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for UtilTiff.
	/// </summary>
	public class UtilTiff
	{
		public UtilTiff()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static bool BmpToTiff(String strSourceBmp, String strOutTif)
		{
			try
			{
				Bitmap bmp = new System.Drawing.Bitmap(strSourceBmp, false);
				EncoderParameters encParams = new EncoderParameters(1);
				System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Compression;
				System.Drawing.Imaging.EncoderParameter encParm = new EncoderParameter(enc, (long)EncoderValue.CompressionCCITT4); //new EncoderParameter(enc, 1, (int)System.Drawing.Imaging.EncoderParameterValueType.ValueTypeLong, (int)System.Drawing.Imaging.EncoderValue.CompressionCCITT4);
				encParams.Param[0] = encParm;
				System.Drawing.Imaging.ImageCodecInfo info = null;

				foreach(ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
				{
					if(ice.MimeType == "image/tiff")
					{
						info = ice;
						break;
					}
				}
				bmp.Save(strOutTif, info, encParams);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static int TiffPageCount(String strTif)
		{
			int intRet;
			Image image;
			
			if(!UtilFile.FileIsThere(strTif) || !UtilString.LikeStr(strTif.ToUpper(), "*.TIF"))
			{
				return 0;
			}
			
			image=Image.FromFile(strTif);
			Guid objGuid=image.FrameDimensionsList[0];
			FrameDimension objDimension=new FrameDimension(objGuid);

			//Gets the total number of frames in the .tiff file
			intRet=image.GetFrameCount(objDimension);
			objDimension = null;
			image.Dispose();
			image = null;
			return intRet;
		}

		public static ArrayList SplitTiffDirectory(String strDir, String strOutDir)
		{
			//on error return empty array
			ArrayList arrRet = new ArrayList();
			if(!UtilFile.FileIsThere(strOutDir))
			{
				if(!UtilFile.MakePath(strOutDir))
				{
					return arrRet;
				}
			}
			ArrayList arrFile = new ArrayList();
			clsDirectory cDir = new clsDirectory(strDir);
			if(!cDir.Populate(false))
			{
				return arrRet;
			}
			if(!cDir.GetArrFile(ref arrFile, "tif", false))
			{
				return arrRet;
			}
			if(arrFile.Count == 0)
			{
				return arrRet;
			}
			ArrayList arrTemp = new ArrayList();
			foreach(String strFile in arrFile)
			{
				arrTemp.Clear();
				arrTemp = SplitTiffImage(strFile, strOutDir, EncoderValue.CompressionCCITT4);
				foreach(String str in arrTemp)
				{
					arrRet.Add(str);
				}
			}
			return arrRet;
		}

		public static ArrayList SplitTiffImage(String strImage, String outPutDirectory, EncoderValue format)
		{
			return SplitTiffImage(strImage, outPutDirectory, format, true);
		}

		public static ArrayList SplitTiffImage(String strImage, String outPutDirectory, EncoderValue format, bool boolPadZeroes)
		{
			//return array of image paths that have been output
			int intPageNumber;
			ArrayList splittedFileNames=new ArrayList();
			intPageNumber = TiffPageCount(strImage);
			if(intPageNumber <= 0)
			{
				return splittedFileNames;
			}

			Image image = Image.FromFile(strImage);
			outPutDirectory = UtilFile.DirWithSlash(outPutDirectory);

			//return an array of image names
			String fileStartString=outPutDirectory + UtilFile.FileFromDir(strImage);
			fileStartString = UtilFile.NameMinusExtension(fileStartString);

			try
			{
				Guid objGuid=image.FrameDimensionsList[0];
				FrameDimension objDimension=new FrameDimension(objGuid);

				//Saves every frame as a separate file.
				Encoder enc=Encoder.Compression;
				int curFrame=0;
				for (int i=0;i<intPageNumber;i++)
				{
					image.SelectActiveFrame(objDimension,curFrame);
					EncoderParameters ep=new EncoderParameters(1);
					ep.Param[0]=new EncoderParameter(enc,(long)format);
					ImageCodecInfo info=GetEncoderInfo("image/tiff");
					
					String fileName = fileStartString;
					if(boolPadZeroes)
					{
						fileName += "_";
						// pad with leading 0's
						if(intPageNumber < 1000)
							fileName += String.Format("{0:000}", i+1);
						else if(intPageNumber < 10000)
							fileName += String.Format("{0:0000}", i+1);
						else if(intPageNumber < 100000)
							fileName += String.Format("{0:00000}", i+1);
						else
							fileName += String.Format("{0:000000}", i+1);
					}
					else
					{
						//Save the master bitmap
						fileName += "_" + Convert.ToInt32(i + 1).ToString();  //String.Format("{0}{1}.TIF",fileStartString,i.ToString());
					}

					fileName += ".tif";
					if(UtilFile.FileIsThere(fileName))
					{
						UtilFile.KillFile(fileName);
					}
					image.Save(fileName,info,ep);
					splittedFileNames.Add(fileName);

					curFrame++;
				}	
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

			image.Dispose();
			image = null;

			return splittedFileNames;
		}

		public static ImageCodecInfo GetEncoderInfo(string mimeType)
		{
			ImageCodecInfo[] encoders=ImageCodecInfo.GetImageEncoders();
			for (int j=0;j<encoders.Length;j++)
			{
				if (encoders[j].MimeType.ToUpper() ==mimeType.ToUpper())
					return encoders[j];
			}
			
			throw new Exception( mimeType + " mime type not found in ImageCodecInfo" );
		}


//		public static bool TiffPageExtract(String strTif, String strTifTo, int intPage)
//		{
//			//for use with the tiffkit dll
//			TiffDLL.ClsTiffDLL obj = new TiffDLL.ClsTiffDLLClass();
//			String strTiff = @"source=" + strTif + ",dest=" + strTifTo + ",save=1,pg=" + intPage.ToString() + ",format=500";
//			int intRet = obj.ConvertImage(strTiff);
//			return (intRet == 0);
//		}
	}
}
