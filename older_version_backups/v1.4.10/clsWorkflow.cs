using System;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsWorkflow.
	/// </summary>
	public class clsWorkflow
	{
		//'***BASE Properties:
		public String strWorkflow;
		private OleDbConnection cnWorkflow;
		public clsBatch clBatchCurr;
		public bool boolConnected;
		public bool boolSQLWF = false;
		public int intPreformStatus = 10;
		public string strUserID = "";
		public string strPassword = "";
		public string strSQLServer = "";

		public clsWorkflow(String strworkflow) : this(strworkflow, false)
		{
		}

		public clsWorkflow(String strworkflow, bool boolConnect)
		{
			this.strWorkflow = strworkflow;
			if(boolConnect)
			{
				Connect();
			}
		}
		
		public OleDbConnection GetConnection()
		{
			return cnWorkflow;
		}
		
		public OleDbConnection cnworkflow
		{
			get
			{
				return cnWorkflow;
			}
		}

		public bool Connect()
		{
			if(boolSQLWF && clsADONET.OleDBOpen(out cnWorkflow, strWorkflow, strUserID, strPassword, strSQLServer))
			{
				boolConnected = true;
			}
			else if(!boolSQLWF && clsADONET.OleDBOpen(out cnWorkflow, strWorkflow))
			{
				boolConnected = true;
			}
			else
			{
				boolConnected = false;
			}
			return boolConnected;
		}

		public void Disconnect()
		{
			if (cnWorkflow != null)
			{
					UpdateBusy(ref cnWorkflow, intPreformStatus);
			}
			clsADONET.fullyCloseDB(ref cnWorkflow);
			boolConnected = false;
		}

		public String AddBatch(clsBatch cBatch, int intStartStatus, String strBatchDate, 
								int intPriority, String strBatchType)
		{
			if(ContainsBatch(cBatch.strBatch))
			{
				return "ERROR: " + UtilFile.NameMinusExtension(cBatch.strBatch) + " already exists in workflow : " + strWorkflow;
			}
			String SQL;
			String strRet="";
			SQL = clsADONET.GetSQL("Workflow", "[Workflow].[BatchName]='" + cBatch.strBatch + "'");
			OleDbDataAdapter adapt = new OleDbDataAdapter(SQL, cnWorkflow);
			OleDbCommandBuilder cb = new OleDbCommandBuilder(adapt);
			cb.QuotePrefix = "[";
			cb.QuoteSuffix = "]";
			DataSet ds = new DataSet();
			adapt.Fill(ds);
			DataTable dTbl = ds.Tables[0];
			
			if(dTbl.Rows.Count > 0)
			{
				strRet = "ERROR: Batch already exists in workflow : " + cBatch.strBatch;
				goto EndFunction;
			}
			intPreformStatus = intStartStatus;

			DataRow dr = dTbl.NewRow();
			dr["BatchName"] = cBatch.strBatch;
			dr["BatchStatus"] = intStartStatus;
			dr["Busy"] = 82;
			dr["Priority"] = intPriority;
			dr["BatchDate"] = strBatchDate;
			dr["CurrentPosition"] = 0;

			if(strBatchType != "")
				if(dr.Table.Columns.Contains("BatchType"))
					dr["BatchType"] = strBatchType;

			dTbl.Rows.Add(dr);
			adapt.Update(dTbl);
			
		EndFunction:
			ds.Dispose();
			cb.Dispose();
			adapt.Dispose();
			return strRet;
		}

		public bool ContainsBatch(String strBatch)
		{
			String SQL;
			String strLike;
			bool boolRet = true;
			strLike = UtilFile.NameMinusExtension(strBatch);
			SQL = clsADONET.GetSQL("Workflow", "[Workflow].[BatchName] Like '%" + strLike + "'");
			OleDbCommand cmd = new OleDbCommand(SQL, cnWorkflow);
			OleDbDataReader rd = cmd.ExecuteReader();

			if(!rd.HasRows)
			{
				boolRet = false;
			}

			rd.Close();
			cmd.Dispose();
			return boolRet;
		}
		public void UpdateBusy(ref OleDbConnection cnWorkflow, int intPreformStatus)
		{
			String SQL;
			//String strLike;
			//bool boolRet = true;
			//strLike = UtilFile.NameMinusExtension(strWorkflow);
			SQL = "UPDATE [WorkFlow] SET Busy = 0 WHERE Busy = 82 AND BatchStatus= " + intPreformStatus;
			//SQL = clsADONET.GetSQL("Workflow", "[Workflow].[BatchName] Like '%" + strLike + "'");
			OleDbCommand cmd = new OleDbCommand(SQL, cnWorkflow);
			OleDbDataReader rd = cmd.ExecuteReader();

			//if(!rd.HasRows)
			//{
			//	boolRet = false;
			//}

			//rd.Close();
			cmd.Dispose();
			//return boolRet;
		}
	}
}
