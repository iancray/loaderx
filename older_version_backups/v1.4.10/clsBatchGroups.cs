using System;
using System.Collections;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsBatchGroups.
	/// </summary>
	public class clsBatchGroups
	{
		public ArrayList arrBatchGroup;
		public clsBatchGroups()
		{
			//
			// TODO: Add constructor logic here
			//
			arrBatchGroup = new ArrayList();
		}

		public bool BatchGroupExists(String strBatchGroup)
		{
			clsBatchGroup cBG = GetBatchGroup(strBatchGroup);
			return (cBG != null);
		}

		public clsWorkflow GetExistingWorkflow(String strWorkflow)
		{
			foreach(clsBatchGroup cBGroup in arrBatchGroup)
			{
				if(cBGroup.cWorkflow.strWorkflow.ToUpper() == strWorkflow.ToUpper())
				{
					return cBGroup.cWorkflow;
				}
			}
			return null;
		}

		public clsWorkflow GetBatchGroupWorkflow(String strBatchGroup)
		{
			clsBatchGroup cBG = GetBatchGroup(strBatchGroup);
			if(cBG == null)
			{
				return null;
			}else
			{
				return cBG.cWorkflow;
			}
		}
		
		public clsBatchGroup GetBatchGroup(String strBatchGroup)
		{
			foreach(clsBatchGroup cBG in arrBatchGroup)
			{
				if(cBG.strBatchGroup.ToUpper() == strBatchGroup.ToUpper())
				{
					return cBG;
				}
			}
			return null;
		}
		
		public ArrayList BatchGroupsFromImageFolder(String strFolder)
		{
			//return an arraylist of matching batch groups
			ArrayList arrRet = new ArrayList();
			foreach(clsBatchGroup cBG in arrBatchGroup)
			{
				if(cBG.FolderMatchesGroup(strFolder))
				{
					arrRet.Add(cBG);
				}
			}
			return arrRet;
		}

	}
}
