using System;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for UtilDate.
	/// </summary>
	public class UtilDate
	{
		public enum DateInterval
		{
			Second, Minute, Hour, Day, Week, Month, Quarter, Year
		}

		public UtilDate()
		{
		}

        public static String DateDDMMMYYConvert(String strDDMMMYY)
        {
            String strMonth;
            String strYear;
            String strDay;
            string retVal = strDDMMMYY;

            strDDMMMYY = UtilString.ReplacePunct(strDDMMMYY, "").ToUpper();

            if (UtilString.LikeStr(strDDMMMYY, "##[A-Z][A-Z][A-Z]##"))
            {
                strMonth = UtilString.Mid(strDDMMMYY, 3, 3);
                strYear = UtilString.Mid(strDDMMMYY, 6, 2);
                strDay = UtilString.Mid(strDDMMMYY, 1, 2);
            }
            else if (UtilString.LikeStr(strDDMMMYY, "##[A-Z][A-Z][A-Z]#"))
            {
                strMonth = UtilString.Mid(strDDMMMYY, 3, 3);
                strYear = "0" + UtilString.Mid(strDDMMMYY, 6, 1);
                strDay = UtilString.Mid(strDDMMMYY, 1, 2);
            }
            else if (UtilString.LikeStr(strDDMMMYY, "#[A-Z][A-Z][A-Z]##"))
            {
                strMonth = UtilString.Mid(strDDMMMYY, 2, 3);
                strYear = UtilString.Mid(strDDMMMYY, 5, 2);
                strDay = "0" + UtilString.Mid(strDDMMMYY, 1, 1);
            }
            else if (UtilString.LikeStr(strDDMMMYY, "#[A-Z][A-Z][A-Z]#"))
            {
                strMonth = UtilString.Mid(strDDMMMYY, 2, 3);
                strYear = "0" + UtilString.Mid(strDDMMMYY, 5, 1);
                strDay = "0" + UtilString.Mid(strDDMMMYY, 1, 1);
            }
            else
            {
                return "";
            }

            if (Convert.ToInt32(strYear) > DateTime.Today.Year - 2000 + 1)
            {
                strYear = "19" + strYear;
            }
            else
            {
                strYear = "20" + strYear;
            }

            switch (strMonth.ToUpper())
            {
                case "JAN":
                    strMonth = "01";
                    break;
                case "FEB":
                    strMonth = "02";
                    break;
                case "MAR":
                    strMonth = "03";
                    break;
                case "APR":
                    strMonth = "04";
                    break;
                case "MAY":
                    strMonth = "05";
                    break;
                case "JUN":
                    strMonth = "06";
                    break;
                case "JUL":
                    strMonth = "07";
                    break;
                case "AUG":
                    strMonth = "08";
                    break;
                case "SEP":
                    strMonth = "09";
                    break;
                case "OCT":
                    strMonth = "10";
                    break;
                case "NOV":
                    strMonth = "11";
                    break;
                case "DEC":
                    strMonth = "12";
                    break;
                default:
                    return "";
            }
            retVal = strMonth + "/" + strDay + "/" + strYear;
            return retVal;
        }

		public static bool DateIsEmpty(DateTime dtDate)
		{
			if((dtDate.Day == 1 && dtDate.Month == 1) && (dtDate.Year == 1900 || dtDate.Year == 1901)) return true;
			return false;
		}
		
		public static bool DateIsEmpty(string strDate)
		{
			DateTime dtDate = DateGetDateTime(strDate);
			return DateIsEmpty(dtDate);
		}
		
		public static DateTime GetEmptyDate()
		{
			return new DateTime(1900, 1, 1);
		}
		
		public static bool DateIsPastDate(String strCurr, String strCheckIfPastOther)
		{
			DateTime dtCurr = DateGetDateTime(strCurr);
			DateTime dtCheckIfPastOther = DateGetDateTime(strCheckIfPastOther);
			return DateIsPastDate(dtCurr, dtCheckIfPastOther);
		}

		public static bool DateIsPastDate(DateTime dtCurr, DateTime dtCheckIfPastOther)
		{
			if(dtCurr.Equals(dtCheckIfPastOther))
			{
				return false;
			}

			TimeSpan dtTemp = dtCurr.Subtract(dtCheckIfPastOther);

			if(Convert.ToInt32(dtTemp.TotalDays) < 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Get a date time object from a date stringstrDate must look like : 10/20/2006 9:30 PM OR just 9:30 PM (will be 1/1/1900 9:30 pm)
		///  or 10202006 or 102006 or 10/20/2006, etc
		/// </summary>
		/// <param name="strDate"></param>
		/// <returns>a datetime object (1/1/1900 if invalid strDate)</returns>
		public static DateTime DateGetDateTime(String strDate)
		{
			//initial cleanup
			strDate = strDate.Trim();		
			while(strDate.IndexOf("  ") >=0)
			{
				strDate = strDate.Replace("  ", " ");
			}
			
			if(UtilString.LikeStr(strDate, "## ## ####") || UtilString.LikeStr(strDate, "## ## ##") ||
				UtilString.LikeStr(strDate, "#### ## ##"))
			{
				strDate = strDate.Replace(" ", "/");
			}
			
			DateTime dtRet;
			string strTime = UtilString.DelimField(strDate, " ", 2);
			bool boolTime = UtilString.LikeStr(strTime, "#:##") || UtilString.LikeStr(strTime, "##:##");
			bool boolPM,  boolMilitary = false;
			int intHours = 0, intMinutes = 0;
			
			
			string strDateOnly = UtilString.DelimField(strDate, " ", 1);
			strDateOnly = DateGetNewFormat(strDateOnly);
			try
			{
				dtRet = Convert.ToDateTime(strDateOnly);
				if(!boolTime)
				{
					return dtRet;
				}
				else
				{
					//now we have to get the time in here
					boolPM = strDate.ToUpper().EndsWith("PM");
					if(!boolPM)
					{
						boolMilitary = !strDate.ToUpper().EndsWith("AM");
					}
					
					intHours = Convert.ToInt32(UtilString.DelimField(strTime, ":", 1));
					intMinutes = Convert.ToInt32(UtilString.DelimField(strTime, ":", 2));
					
					if(boolPM && intHours < 12)
					{
						intHours += 12;
					}
					else if(!boolPM && !boolMilitary && intHours == 12)
					{
						intHours = 0; //12 AM is 00 military
					}
					
					dtRet = dtRet.AddHours(intHours);
					dtRet = dtRet.AddMinutes(intMinutes);
					return dtRet;
				}
			}
			catch
			{
				return new DateTime(1900, 1, 1);
			}
		}

		/// <summary>
		/// return a string date in the format: mm/dd/ccyy. 6 dig dates assume mmddyy
		/// </summary>
		/// <param name="strDate"></param>
		/// <returns></returns>
		public static String DateGetNewFormat(String strDate)
		{
			string strRet;
			if(UtilString.LikeStr(strDate, "######"))
			{
				strRet = DateGetNewFormat(strDate, "MMDDYY", "MM/DD/CCYY");
			}
			else if(UtilString.LikeStr(strDate, "########"))
			{
				if(strDate.StartsWith("19") || strDate.StartsWith("20"))
				{
					strRet = DateGetNewFormat(strDate, "CCYYMMDD", "MM/DD/CCYY");
				}else
				{
					strRet = DateGetNewFormat(strDate, "MMDDCCYY", "MM/DD/CCYY");
				}
			}
			else
			{
				strRet = DateGetNewFormat(strDate, "", "");
			}
			return strRet;
		}

		public static String DateGetNewFormat(String strDate, String strSourceFormat, String strFormatTo)
		{
			int intCentCutoff = DateTime.Today.Year + 3 - 2000;
			return DateGetNewFormat(strDate, strSourceFormat, strFormatTo, intCentCutoff);
		}
		
		/// <summary>
		/// Reformat any date any way you like.  If strDate is delimited by '/' then sourceformat, strformatto not required (just pass in blank)
		/// </summary>
		/// <param name="strDate">ie "121006"</param>
		/// <param name="strSourceFormat">ie "MMDDYY".  If strsourceformat.length != strDate.length then 1/1/1900 is returned</param>
		/// <param name="strFormatTo">ie "MMDDCCYY".. to force a century u can do: "MMDD20YY" -- can also specify "Julian" </param>
		/// <returns></returns>
		public static String DateGetNewFormat(String strDate, String strSourceFormat, String strFormatTo,
			int intCenturyCutOffYear)
		{
			strSourceFormat = strSourceFormat.ToUpper();
			strFormatTo = strFormatTo.ToUpper();
			if(strFormatTo == "")
			{
				strFormatTo = "MM/DD/CCYY";
			}
			int intCent, intMonth, intYear, intDay;
			string strRet, strTemp;
			
			strDate = strDate.Trim();
			strDate = strDate.Replace("-", "/");
			strDate = strDate.Replace(" ", "/");
			
			if(UtilString.Count(strDate, "/") == 2)
			{
				if(UtilString.LikeStr(strDate, "##/##/####") || UtilString.LikeStr(strDate, "#/##/####") ||
					UtilString.LikeStr(strDate, "##/#/####") || UtilString.LikeStr(strDate, "#/#/####"))
				{
					//we have mm/dd/ccyy
					strTemp = UtilString.DelimField(strDate, "/", 3);
					intCent = Convert.ToInt32(strTemp.Substring(0, 2));
					intYear = Convert.ToInt32(strTemp.Substring(2, 2));
					intMonth = Convert.ToInt32(UtilString.DelimField(strDate, "/", 1));
					intDay = Convert.ToInt32(UtilString.DelimField(strDate, "/", 2));
				}
				else if(UtilString.LikeStr(strDate, "####/##/##") || UtilString.LikeStr(strDate, "####/#/##") ||
					UtilString.LikeStr(strDate, "####/##/#") || UtilString.LikeStr(strDate, "####/#/#"))
				{
					//we have ccyy/mm/dd
					strTemp = UtilString.DelimField(strDate, "/", 1);
					intCent = Convert.ToInt32(strTemp.Substring(0, 2));
					intYear = Convert.ToInt32(strTemp.Substring(2, 2));
					intMonth = Convert.ToInt32(UtilString.DelimField(strDate, "/", 2));
					intDay = Convert.ToInt32(UtilString.DelimField(strDate, "/", 3));
				}
				else if(UtilString.LikeStr(strDate, "##/##/##") || UtilString.LikeStr(strDate, "#/##/##") ||
					UtilString.LikeStr(strDate, "##/#/##") || UtilString.LikeStr(strDate, "#/#/##"))
				{
					//we have mm/dd/yy
					intYear = Convert.ToInt32(UtilString.DelimField(strDate, "/", 3));
					intMonth = Convert.ToInt32(UtilString.DelimField(strDate, "/", 1));
					intDay = Convert.ToInt32(UtilString.DelimField(strDate, "/", 2));
					
					if(intYear < intCenturyCutOffYear)
					{
						intCent = 20;
					}
					else
					{
						intCent = 19;
					}
				}
				else
				{
					//bad date!
					//BAD DATE
					goto BadDate;
				}
			}
			else if(UtilString.Count(strDate, "/") > 0 || strDate.Length != strSourceFormat.Length)
			{
				//BAD DATE
				goto BadDate;
			}
			else if(!UtilString.LikeStr(strDate, "######") && !UtilString.LikeStr(strDate, "########"))
			{
				goto BadDate;
			}
			else
			{
				strSourceFormat = strSourceFormat.Replace("MM", "ab");
				strSourceFormat = strSourceFormat.Replace("DD", "cd");
				strSourceFormat = strSourceFormat.Replace("YY", "ef");
				strSourceFormat = strSourceFormat.Replace("CC", "gh");

				if(strDate.Length == 8)
				{
					strDate = UtilString.StringFormat(strDate, strSourceFormat, "abcdghef");
					intMonth = Convert.ToInt32(strDate.Substring(0, 2));
					intDay = Convert.ToInt32(strDate.Substring(2, 2));
					intYear = Convert.ToInt32(strDate.Substring(6, 2));
					intCent = Convert.ToInt32(strDate.Substring(4, 2));
				}
				else
				{
					strDate = UtilString.StringFormat(strDate, strSourceFormat, "abcdef");
					
					intMonth = Convert.ToInt32(strDate.Substring(0, 2));
					intDay = Convert.ToInt32(strDate.Substring(2, 2));
					intYear = Convert.ToInt32(strDate.Substring(4, 2));
					
					if(intYear < intCenturyCutOffYear)
					{
						intCent = 20;
					}
					else
					{
						intCent = 19;
					}
				}
			}
			
			if(strFormatTo.Trim().ToUpper() == "JULIAN")
			{
				string strCCYY = UtilString.FillChars(intCent.ToString(), "0", 2, true) + UtilString.FillChars(intYear.ToString(), "0", 2, true);
				DateTime dt = new DateTime(Convert.ToInt32(strCCYY), intMonth, intDay);
				strRet = UtilString.FillChars(intYear.ToString(), "0", 2, true) + UtilString.FillChars(dt.DayOfYear.ToString(), "0", 3, true);
			}
			else
			{
				strRet = strFormatTo;
				strRet = strRet.Replace("MM", UtilString.FillChars(intMonth.ToString(), "0", 2, true));
				strRet = strRet.Replace("M", intMonth.ToString()); //for single dig months ie M/D/CCYY
				strRet = strRet.Replace("DD", UtilString.FillChars(intDay.ToString(), "0", 2, true));
				strRet = strRet.Replace("D", intDay.ToString()); //for single dig days ie M/D/CCYY
				strRet = strRet.Replace("CC", UtilString.FillChars(intCent.ToString(), "0", 2, true));
				strRet = strRet.Replace("YYYY", UtilString.FillChars(intCent.ToString(), "0", 2, true) + UtilString.FillChars(intYear.ToString(), "0", 2, true));
				strRet = strRet.Replace("YY", UtilString.FillChars(intYear.ToString(), "0", 2, true));
			}
			
			return strRet;
			
			BadDate:
				return "01/01/1900";
		}

		/// <summary>
		/// Return the month of a specified string date
		/// </summary>
		/// <param name="strDate">Date must start with the month, ie 09012000</param>
		/// <returns>an integer representing the month</returns>
		public static int DateMonth(String strDate)
		{
			//date must be in format MM.... and at least 6 chars (no 4 char dates)
			while(strDate.Length < 6)
			{
				strDate = "0" + strDate;
			}

			return Convert.ToInt32(strDate.Substring(0, 2));
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="strDate">must be in the format ###### or ##/##/####</param>
		/// <returns>returns a string in this format : MMDDYY</returns>
		public static String DateNextBusinessDayMMDDYY(string strDate)
		{
			//next business day after strDate.. strDate must eitehr be in format
			// ###### or ##/##/####
			// IMPORTANT: returns a string in this format : MMDDYY
			if(UtilString.LikeStr(strDate, "######"))
			{
				strDate = strDate.Substring(0, 2) + "/" + strDate.Substring(2, 2) + "/20" + strDate.Substring(4, 2);
			}
			DateTime dt = DateGetDateTime(strDate);
			if(dt.DayOfWeek.ToString().ToUpper() == "FRIDAY")
			{
				//add 3 days
				dt = dt.AddDays(3);
			}
			else if(dt.DayOfWeek.ToString().ToUpper() == "SATURDAY")
			{
				dt = dt.AddDays(2);
			}
			else
			{
				dt = dt.AddDays(1);
			}
			return DateFormat(dt.ToShortDateString(), "MMDDYY");
		}
		
		/// <summary>
		/// Return the year contained in the specified date. If the date is 6 digits
		/// and the year is lower than 50, it will assume Century is 2000, else 1900
		/// </summary>
		/// <param name="strDate">Any string date with a 4 digit year on the end</param>
		/// <returns>The year ie 1999 or 2007</returns>
		public static int DateYear(String strDate)
		{
			//year must be at the end of the string but the date can be
			// 6 or 8 ints (after punct strip)
			strDate = UtilString.ReplaceNonInt(strDate, "", true, true);
			//return something like 1999 or 2007
			while(strDate.Length < 6)
			{
				strDate = "0" + strDate;
			}
			int intTemp;
			if(strDate.Length == 8)
			{
				intTemp = Convert.ToInt32(strDate.Substring(4, 4));
			}
			else
			{
				intTemp = Convert.ToInt32(strDate.Substring(4, 2));
			}
			if(intTemp < 50)
			{
				intTemp += 2000;
			}
			else
			{
				intTemp += 1900;
			}
			return intTemp;
		}
		
		/// <summary>
		/// Get the difference in # of days between 2 dates
		/// </summary>
		/// <returns>difference between 2 dates in days</returns>
		public static int DateDiff(DateTime dtLater, DateTime dtEarlier)
		{
			TimeSpan dtTemp = dtLater.Subtract(dtEarlier);
			int intRet = Convert.ToInt32(dtTemp.TotalDays);
			return intRet;
		}
		
		/// <summary>
		/// Get the difference between 2 dates - units depend on specified interval
		/// </summary>
		/// <returns>difference between 2 dates</returns>
		public static long DateDiff(DateInterval Interval, System.DateTime StartDate, System.DateTime EndDate )
		{
			long lngDateDiffValue = 0;
			System.TimeSpan TS = new System.TimeSpan(EndDate.Ticks - StartDate.Ticks);
			switch (Interval)
			{
				case DateInterval.Day:
					lngDateDiffValue = (long)TS.Days;
					break;
				case DateInterval.Hour:
					lngDateDiffValue = (long)TS.TotalHours;
					break;
				case DateInterval.Minute:
					lngDateDiffValue = (long)TS.TotalMinutes;
					break;
				case DateInterval.Month:
					lngDateDiffValue = (long)(TS.Days / 30);
					break;
				case DateInterval.Quarter:
					lngDateDiffValue = (long)((TS.Days / 30) / 3);
					break;
				case DateInterval.Second:
					lngDateDiffValue = (long)TS.TotalSeconds;
					break;
				case DateInterval.Week:
					lngDateDiffValue = (long)(TS.Days / 7);
					break;
				case DateInterval.Year:
					lngDateDiffValue = (long)(TS.Days / 365);
					break;
			}
			return (lngDateDiffValue);
		}//end of DateDiff
		
		/// <summary>
		/// Reformat a string date
		/// </summary>
		/// <param name="strDate">must look like MM/DD/CCYY or M/D/CCYY, etc</param>
		/// <param name="strFormat">ie MMDDCCYY OR YYMMDD OR CCYY/MM/DD, etc</param>
		/// <returns>Reformatted string date in the specified format</returns>
		public static String DateFormat(String strDate, String strFormat)
		{
			//strDate mustbe in format: MM/DD/CCYY
			// OR M/D/CCYY , etc
			//format looks something like this:
			//  MMDDCCYY OR YYMMDD OR CCYYMMDD or something like that
			String strCent = "", strMonth = "", strDay = "", strYear = "";
			
			
			strMonth = UtilString.DelimField(strDate, "/", 1);
			strDay = UtilString.DelimField(strDate, "/", 2);
			strYear = UtilString.DelimField(strDate, "/", 3);
			if(strMonth.Length == 1)
			{
				strMonth = "0" + strMonth;
			}
			if(strDay.Length == 1)
			{
				strDay = "0" + strDay;
			}
			if(strYear.Length == 2)
			{
				if(UtilString.LikeStr(strYear, "##"))
				{
					if(Convert.ToInt32(strYear) > 30)
					{
						strYear = "19" + strYear;
					}
					else
					{
						strYear = "20" + strYear;
					}
				}
			}
			
			strDate = strMonth + "/" + strDay + "/" + strYear;
			
			if(!UtilString.LikeStr(strDate, "??/??/????"))
			{
				return strDate;
			}
			
			
			strMonth = strDate.Substring(0, 2);
			strDay = strDate.Substring(3, 2);
			strCent = strDate.Substring(6, 2);
			strYear = strDate.Substring(8, 2);
			
			String strRet = strFormat.ToUpper();
			strRet = strRet.Replace("MM", strMonth);
			strRet = strRet.Replace("DD", strDay);
			strRet = strRet.Replace("CC", strCent);
            strRet = strRet.Replace("YYYY", strDate.Substring(6, 4));
			strRet = strRet.Replace("YY", strYear);
			return strRet;
		}

		/// <summary>
		/// Convert a string julian date to a normal string date
		/// </summary>
		/// <param name="strJulian">5 digit julian date : CCjjj</param>
		/// <param name="strCent">First 2 characters of the returned century ie 20 or 19</param>
		/// <returns>String date in the format: mm/dd/ccyy </returns>		
		public static String DateFromJulian(String strJulian)
		{
			//use current century
			String strCent;
			strCent = DateTime.Today.Year.ToString().Substring(0, 2);
			return DateFromJulian(strJulian, strCent);
		}

		/// <summary>
		/// Convert a string julian date to a normal string date
		/// </summary>
		/// <param name="strJulian">5 digit julian date : CCjjj</param>
		/// <param name="strCent">First 2 characters of the returned century ie 20 or 19</param>
		/// <returns>String date in the format: mm/dd/ccyy </returns>
		public static String DateFromJulian(String strJulian, String strCent)
		{
			//5 digit date.. CCjjj
			// returns string date in the format: mm/dd/ccyy 
			// return blank on error
			DateTime bDate;
			String strTemp, strTemp2;
			int intYear, intDayOfYear;
			if(strJulian.Length != 5 || strCent.Length != 2)
			{
				return "";
			}

			strTemp = strJulian;
			strTemp2 = strTemp.Substring(0, 2);
			strTemp = strTemp.Substring(2, 3);
			intYear = Convert.ToInt32(strCent + strTemp2);
			bDate = new DateTime(intYear, 1, 1);
			intDayOfYear = Convert.ToInt32(strTemp) - 1;
			bDate = bDate.AddDays(intDayOfYear);

			strTemp = bDate.ToShortDateString();
			strTemp = DateFormat(strTemp, "MM/DD/CCYY");
			return strTemp;
		}
		
		public static int CompareDateTime(DateTime t1, DateTime t2)
		{// returns 1 if t1 > t2; 0 if t1 = t2; -1 if t1 < t2
			if(t1.Year > t2.Year)
				return 1;
			if(t1.Year < t2.Year)
				return -1;
		
			if(t1.Month > t2.Month)
				return 1;
			if(t1.Month < t2.Month)
				return -1;
			
			if (t1.Day > t2.Day)
				return 1;
			if (t1.Day < t2.Day)
				return -1;				
		
			if (t1.Hour > t2.Hour)
				return 1;
			if (t1.Hour < t2.Hour)
				return -1;
				
			if (t1.Minute > t2.Minute)
				return 1;
			if (t1.Minute < t2.Minute)
				return -1;
				
			if (t1.Second > t2.Second) 
				return 1;
			if (t1.Second < t2.Second) 
				return -1;
		
			
			return 0;
		}
		
		public static int CalculateCurrentAge(DateTime dtBirthDate)
		{
			return CalculateAgeAtDate(dtBirthDate, DateTime.Today);
		}

		public static int CalculateAgeAtDate(DateTime dtBirthDate, DateTime dtDate)
		{
			return Convert.ToInt32(DateDiff(DateInterval.Year, dtBirthDate, dtDate));
		}

		/// <summary>
		/// Gets the date that is intDays ago.
		/// </summary>
		public static DateTime DateDaysAgo(int intDays)
		{
			return DateTime.Today.Subtract(new System.TimeSpan(intDays, 0, 0, 0));
			// could also do: DateTime.Today.AddDays(intDays * -1);
		}

		/// <summary>
		/// -- adjusts path if there is a mask for using a date 
		/// -- square brackets must be used to specify date format 
		/// -- ex. T:\MESA\!Test\Portal\[YYMMDD]\ 
		/// -- can also be [Julian] for a julian date
		/// </summary>
		public static string AdjustDatePath(string strPath) { return AdjustDatePath(strPath, DateTime.Today); }
		public static string AdjustDatePath(string strPath, DateTime dtDate)
		{
			string strRet = strPath;
			int intStart = -1;
			int intEnd = -1;

			intStart = strPath.IndexOf("[");

			if(intStart != -1)
			{
				intEnd = strPath.IndexOf("]");
				if(intEnd != -1)
				{
					string strDateFormat = strPath.Substring(intStart+1, intEnd - (intStart+1));
					string strDate = dtDate.ToShortDateString();
					strDate = UtilDate.DateGetNewFormat(strDate, "MM/DD/YYYY", strDateFormat);
					strRet = UtilString.ReplaceByIndex(strRet, strDate, intStart, intEnd);
				}
			}

			// we can do this recursively if there is still another date mask
			if(strRet.IndexOf("[") != -1)
				strRet = AdjustDatePath(strRet, dtDate);

			return strRet;
		}

		public static string MonthName(int intMonth)
		{
			return System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(intMonth);
		}
	}
}
