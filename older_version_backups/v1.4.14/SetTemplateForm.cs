using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for SetTemplateForm.
	/// </summary>
	public class SetTemplateForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtTemplate;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.RadioButton rbDefault;
		private System.Windows.Forms.RadioButton rbCustom;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public bool UseDefault {get {return rbDefault.Checked;} set{rbDefault.Checked = value; rbCustom.Checked = !value;}}
		public string BatchTemplate {get{return txtTemplate.Text;} set{txtTemplate.Text = value;}}

		public SetTemplateForm()
		{
			// Required for Windows Form Designer support
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.txtTemplate = new System.Windows.Forms.TextBox();
			this.rbCustom = new System.Windows.Forms.RadioButton();
			this.rbDefault = new System.Windows.Forms.RadioButton();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnBrowse);
			this.groupBox1.Controls.Add(this.txtTemplate);
			this.groupBox1.Controls.Add(this.rbCustom);
			this.groupBox1.Controls.Add(this.rbDefault);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(480, 88);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// btnBrowse
			// 
			this.btnBrowse.Location = new System.Drawing.Point(388, 48);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.TabIndex = 4;
			this.btnBrowse.Text = "&Browse";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// txtTemplate
			// 
			this.txtTemplate.Location = new System.Drawing.Point(72, 48);
			this.txtTemplate.Name = "txtTemplate";
			this.txtTemplate.ReadOnly = true;
			this.txtTemplate.Size = new System.Drawing.Size(312, 20);
			this.txtTemplate.TabIndex = 3;
			this.txtTemplate.Text = "";
			// 
			// rbCustom
			// 
			this.rbCustom.Location = new System.Drawing.Point(12, 48);
			this.rbCustom.Name = "rbCustom";
			this.rbCustom.Size = new System.Drawing.Size(64, 24);
			this.rbCustom.TabIndex = 2;
			this.rbCustom.Text = "Custom";
			this.rbCustom.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// rbDefault
			// 
			this.rbDefault.Checked = true;
			this.rbDefault.Location = new System.Drawing.Point(12, 16);
			this.rbDefault.Name = "rbDefault";
			this.rbDefault.Size = new System.Drawing.Size(148, 24);
			this.rbDefault.TabIndex = 1;
			this.rbDefault.TabStop = true;
			this.rbDefault.Text = "Default (from Master DB)";
			this.rbDefault.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(328, 112);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 5;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(408, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// SetTemplateForm
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(496, 142);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.groupBox1);
			this.Name = "SetTemplateForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Set Batch Template";
			this.Load += new System.EventHandler(this.SetTemplateForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			string strTemplate = "";
			strTemplate = UtilMisc.BrowseFile(txtTemplate.Text, "Access Databases (*.mdb)|*.mdb", "Please Select a Batch Template Database");

			if(strTemplate != "")
				txtTemplate.Text = strTemplate;
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if(rbCustom.Checked)
			{
				string strError = "";

				// make sure file exists
				if(!UtilFile.FileIsThere(txtTemplate.Text)) 
				{
					strError = "Error: Could not locate file " + txtTemplate.Text;
				}

				if(strError == "")
				{
					// make sure it's a valid batch (for now just check for document table)
					clsBatch cBatch = new clsBatch(txtTemplate.Text);
					if(!cBatch.OpenBatch())
						strError = "Could not validate batch: " + txtTemplate.Text;
					else if(!UtilDB.TableIsThere(cBatch.cnBatch, "Document"))
						strError = "Invalid batch: " + txtTemplate.Text;

					cBatch.CloseBatch();
					cBatch = null;
				}

				if(strError != "") 
				{
					MessageBox.Show(strError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}

			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void rb_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbDefault.Checked)
			{
				btnBrowse.Enabled = false;
				txtTemplate.Enabled = false;
			}
			else if(rbCustom.Checked)
			{
				btnBrowse.Enabled = true;
				txtTemplate.Enabled = true;
			}
		}

		private void SetTemplateForm_Load(object sender, System.EventArgs e)
		{
			if(rbDefault.Checked)
			{
				btnBrowse.Enabled = false;
				txtTemplate.Enabled = false;
			}
			else if(rbCustom.Checked)
			{
				btnBrowse.Enabled = true;
				txtTemplate.Enabled = true;
			}
		}

	}
}
