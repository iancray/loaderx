using System;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for clsProgramOptions.
	public class clsProgramOptions : clsOptions 
	{
		public clsProgramOptions()
		{
			// TODO: Add constructor logic here
		}

		public bool PopulateFromValMan(ADODB.Connection cnValMan, String strProgramName)
		{
			ADODB.Recordset rsRec;

			if(!UtilADO.TableIsThere(cnValMan, "ProgramOptions"))
			{
				//assuming this program doesn't require options
				return true;
			}
			
			rsRec = UtilADO.RecSetCond(cnValMan, "ProgramOptions", "[ProgramOptions].[ProgramName]='" + strProgramName + "'");
			clsOption cOption;
			while(!rsRec.EOF)
			{
				clsDocuProgram.DoEvents();
				cOption = new clsOption();
				cOption.strDescription = (String)rsRec.Fields["Description"].Value;
				cOption.strOption = (String)rsRec.Fields["Option"].Value;
				cOption.strOtherParameters = (String)rsRec.Fields["OtherParam"].Value;
				cOption.strParameter = (String)rsRec.Fields["Parameter"].Value;
				cOption.strProgramName = strProgramName;
				base.AddOption(cOption);

				rsRec.MoveNext();
			}

			UtilADO.fullyCloseRS(ref rsRec);
			return true;
		}
	}
}
