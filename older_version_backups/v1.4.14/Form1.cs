using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace DocustreamNS
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		#region Windows Form Designer generated code

		internal System.Windows.Forms.ProgressBar ProgressBar1;
		public System.Windows.Forms.TextBox txtStatus;
		public System.Windows.Forms.Label lblStatus;
		public System.Windows.Forms.Button btnStop;
		public System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		public System.Windows.Forms.ListBox lstFolderPaths;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.Button btnSelectAll;
		public System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.DateTimePicker dtBatchDate;
		public System.Windows.Forms.Button btnViewLog;
		public System.Windows.Forms.Label label3;
		public System.Windows.Forms.Button btnViewWF;
		public System.Windows.Forms.TextBox txtMaster;
		public System.Windows.Forms.Button btnTemplate;
		private System.Windows.Forms.Timer autoStartTimer;
		private System.ComponentModel.IContainer components;
		public System.Windows.Forms.Button btnExit;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				
			}
			base.Dispose( disposing );
		}

		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnStart = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.lblStatus = new System.Windows.Forms.Label();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.lstFolderPaths = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSelectAll = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.dtBatchDate = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.btnViewLog = new System.Windows.Forms.Button();
			this.txtMaster = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btnViewWF = new System.Windows.Forms.Button();
			this.btnTemplate = new System.Windows.Forms.Button();
			this.autoStartTimer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// ProgressBar1
			// 
			this.ProgressBar1.Location = new System.Drawing.Point(4, 248);
			this.ProgressBar1.Name = "ProgressBar1";
			this.ProgressBar1.Size = new System.Drawing.Size(468, 16);
			this.ProgressBar1.TabIndex = 39;
			// 
			// btnStop
			// 
			this.btnStop.BackColor = System.Drawing.SystemColors.Control;
			this.btnStop.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnStop.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnStop.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnStop.Location = new System.Drawing.Point(476, 212);
			this.btnStop.Name = "btnStop";
			this.btnStop.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnStop.Size = new System.Drawing.Size(76, 24);
			this.btnStop.TabIndex = 33;
			this.btnStop.Text = "S&top";
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// btnStart
			// 
			this.btnStart.BackColor = System.Drawing.SystemColors.Control;
			this.btnStart.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnStart.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnStart.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnStart.Location = new System.Drawing.Point(476, 184);
			this.btnStart.Name = "btnStart";
			this.btnStart.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnStart.Size = new System.Drawing.Size(76, 24);
			this.btnStart.TabIndex = 32;
			this.btnStart.Text = "Load";
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// btnExit
			// 
			this.btnExit.BackColor = System.Drawing.SystemColors.Control;
			this.btnExit.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnExit.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnExit.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnExit.Location = new System.Drawing.Point(476, 240);
			this.btnExit.Name = "btnExit";
			this.btnExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnExit.Size = new System.Drawing.Size(76, 24);
			this.btnExit.TabIndex = 31;
			this.btnExit.Text = "E&xit";
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// txtStatus
			// 
			this.txtStatus.AcceptsReturn = true;
			this.txtStatus.AutoSize = false;
			this.txtStatus.BackColor = System.Drawing.SystemColors.Control;
			this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtStatus.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtStatus.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtStatus.Location = new System.Drawing.Point(68, 228);
			this.txtStatus.MaxLength = 0;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.ReadOnly = true;
			this.txtStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtStatus.Size = new System.Drawing.Size(400, 17);
			this.txtStatus.TabIndex = 28;
			this.txtStatus.TabStop = false;
			this.txtStatus.Text = "";
			// 
			// lblStatus
			// 
			this.lblStatus.BackColor = System.Drawing.SystemColors.Control;
			this.lblStatus.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStatus.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblStatus.Location = new System.Drawing.Point(4, 228);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblStatus.Size = new System.Drawing.Size(56, 17);
			this.lblStatus.TabIndex = 38;
			this.lblStatus.Text = "Status:";
			this.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lstFolderPaths
			// 
			this.lstFolderPaths.Location = new System.Drawing.Point(4, 20);
			this.lstFolderPaths.Name = "lstFolderPaths";
			this.lstFolderPaths.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lstFolderPaths.Size = new System.Drawing.Size(468, 186);
			this.lstFolderPaths.TabIndex = 42;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(4, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(260, 16);
			this.label1.TabIndex = 43;
			this.label1.Text = "Folders containing images:    (drag and drop)";
			// 
			// btnSelectAll
			// 
			this.btnSelectAll.BackColor = System.Drawing.SystemColors.Control;
			this.btnSelectAll.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnSelectAll.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSelectAll.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnSelectAll.Location = new System.Drawing.Point(476, 20);
			this.btnSelectAll.Name = "btnSelectAll";
			this.btnSelectAll.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnSelectAll.Size = new System.Drawing.Size(76, 24);
			this.btnSelectAll.TabIndex = 44;
			this.btnSelectAll.Text = "Select All";
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.BackColor = System.Drawing.SystemColors.Control;
			this.btnRemove.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnRemove.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnRemove.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnRemove.Location = new System.Drawing.Point(476, 48);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnRemove.Size = new System.Drawing.Size(76, 24);
			this.btnRemove.TabIndex = 45;
			this.btnRemove.Text = "Remove";
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// dtBatchDate
			// 
			this.dtBatchDate.Enabled = false;
			this.dtBatchDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtBatchDate.Location = new System.Drawing.Point(384, 0);
			this.dtBatchDate.Name = "dtBatchDate";
			this.dtBatchDate.Size = new System.Drawing.Size(88, 20);
			this.dtBatchDate.TabIndex = 46;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(348, 4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(36, 16);
			this.label2.TabIndex = 47;
			this.label2.Text = "Date:";
			// 
			// btnViewLog
			// 
			this.btnViewLog.BackColor = System.Drawing.SystemColors.Control;
			this.btnViewLog.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnViewLog.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnViewLog.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnViewLog.Location = new System.Drawing.Point(476, 148);
			this.btnViewLog.Name = "btnViewLog";
			this.btnViewLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnViewLog.Size = new System.Drawing.Size(76, 24);
			this.btnViewLog.TabIndex = 48;
			this.btnViewLog.Text = "View Log";
			this.btnViewLog.Click += new System.EventHandler(this.btnViewLog_Click);
			// 
			// txtMaster
			// 
			this.txtMaster.AcceptsReturn = true;
			this.txtMaster.AutoSize = false;
			this.txtMaster.BackColor = System.Drawing.SystemColors.Control;
			this.txtMaster.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtMaster.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.txtMaster.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtMaster.ForeColor = System.Drawing.SystemColors.WindowText;
			this.txtMaster.Location = new System.Drawing.Point(68, 208);
			this.txtMaster.MaxLength = 0;
			this.txtMaster.Name = "txtMaster";
			this.txtMaster.ReadOnly = true;
			this.txtMaster.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.txtMaster.Size = new System.Drawing.Size(400, 17);
			this.txtMaster.TabIndex = 49;
			this.txtMaster.TabStop = false;
			this.txtMaster.Text = "";
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.SystemColors.Control;
			this.label3.Cursor = System.Windows.Forms.Cursors.Default;
			this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label3.Location = new System.Drawing.Point(4, 208);
			this.label3.Name = "label3";
			this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label3.Size = new System.Drawing.Size(56, 17);
			this.label3.TabIndex = 50;
			this.label3.Text = "Master:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnViewWF
			// 
			this.btnViewWF.BackColor = System.Drawing.SystemColors.Control;
			this.btnViewWF.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnViewWF.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnViewWF.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnViewWF.Location = new System.Drawing.Point(476, 84);
			this.btnViewWF.Name = "btnViewWF";
			this.btnViewWF.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnViewWF.Size = new System.Drawing.Size(76, 24);
			this.btnViewWF.TabIndex = 51;
			this.btnViewWF.Text = "Set Master...";
			this.btnViewWF.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnTemplate
			// 
			this.btnTemplate.BackColor = System.Drawing.SystemColors.Control;
			this.btnTemplate.Cursor = System.Windows.Forms.Cursors.Default;
			this.btnTemplate.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnTemplate.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnTemplate.Location = new System.Drawing.Point(476, 112);
			this.btnTemplate.Name = "btnTemplate";
			this.btnTemplate.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnTemplate.Size = new System.Drawing.Size(76, 24);
			this.btnTemplate.TabIndex = 52;
			this.btnTemplate.Text = "Set Template...";
			this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
			// 
			// autoStartTimer
			// 
			this.autoStartTimer.Interval = 10;
			this.autoStartTimer.Tick += new System.EventHandler(this.autoStartTimer_Tick);
			// 
			// Form1
			// 
			this.AllowDrop = true;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(558, 268);
			this.Controls.Add(this.btnTemplate);
			this.Controls.Add(this.btnViewWF);
			this.Controls.Add(this.txtMaster);
			this.Controls.Add(this.txtStatus);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnViewLog);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dtBatchDate);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnSelectAll);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lstFolderPaths);
			this.Controls.Add(this.ProgressBar1);
			this.Controls.Add(this.btnStop);
			this.Controls.Add(this.btnStart);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.lblStatus);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "Form1";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "DocuLoaderX (0.0.0)";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		public clsDocuProgram clDocuprogram;
		public bool boolAutoStart = false;
		public bool boolSuppressMessages = false;
		public string strCmdLineLog = "";
		public string strCmdLineBatchType = "";

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// set form title to assembly name and version
			string strProgramName = UtilMisc.GetAssemblyName();
			string strProgramVersion = UtilMisc.GetAssemblyVersionNoBuild();
			this.Text = strProgramName + " (" + strProgramVersion + ")";

			string strMaster = "";
			getCommandLine(ref strMaster);
			
			if(strMaster == "")
				strMaster = UtilString.GetSetting("DocuLoaderX", "Master");

			clDocuprogram = new clsDocuProgram();
			if(!clDocuprogram.Init("DocuLoaderX", this, strMaster, strCmdLineLog))
			{	
				btnStart.Enabled = false;
			}else
			{
				txtMaster.Text = clsDocuProgram.cOptions.strValMan;
			}

			if(boolAutoStart)
			{
				autoStartTimer.Enabled = true;
				clDocuprogram.boolRunByAutostart = true; // 1.4.14
			}
		}

		private void getCommandLine(ref string strMaster)
		{
			string strError = "";
			string[] args = Environment.GetCommandLineArgs();
			string strParentDir = "";

			for(int i=0; i < args.Length; i++)
			{
				string strArg = args[i].ToLower();
				switch(strArg)
				{
					case "-parentdir":
					case "/parentdir":
						if(i == args.Length - 1 || args[i+1].StartsWith("-") || args[i+1].StartsWith("/"))
							strError += "COMMAND LINE ERROR: parent directory not correctly specified." + Environment.NewLine;
						else
							strParentDir = UtilFile.DirWithSlash(args[++i]);
						break;
					case "-dir":
					case "/dir":
						if(i == args.Length - 1 || args[i+1].StartsWith("-") || args[i+1].StartsWith("/"))
							strError += "COMMAND LINE ERROR: directory not correctly specified." + Environment.NewLine;
						else
							lstFolderPaths.Items.Add(strParentDir + args[++i]);
						break;
					case "-log":
					case "/log":
						if(i == args.Length - 1 || args[i+1].StartsWith("-") || args[i+1].StartsWith("/"))
							strError += "COMMAND LINE ERROR: log file not correctly specified." + Environment.NewLine;
						else
							strCmdLineLog = args[++i];
						break;
					case "-master":
					case "/master":
						if(i == args.Length - 1 || args[i+1].StartsWith("-") || args[i+1].StartsWith("/"))
							strError += "COMMAND LINE ERROR: master database not correctly specified." + Environment.NewLine;
						else
							strMaster = args[++i];
						break;
					case "-batchtype":
					case "/batchtype":
						if(i == args.Length - 1 || args[i+1].StartsWith("-") || args[i+1].StartsWith("/"))
							strError += "COMMAND LINE ERROR: log file not correctly specified." + Environment.NewLine;
						else
							strCmdLineBatchType = args[++i];
						break;
					case "-nopopup":
					case "/nopopup":
						boolSuppressMessages = true;
						break;
					case "-autostart":
					case "/autostart":
						boolAutoStart = true;
						break;
				}
			}

			if(strError != "")
			{
				ShowCommandLineError(strError);

				// close program
				btnExit_Click(this, System.EventArgs.Empty);
			}
		}

		private void ShowCommandLineError(string strErr)
		{
			string strError = "Command Line: " + Environment.CommandLine + "\r\n\r\n" + strErr + "\r\n\r\n";
			strError += "Usage: " + "\r\n\r\n";
			strError += "-parentdir, /parentdir" + "\t" + "Parent Folder -- Path of parent directory for image folders, required if not specifying full path for image folders.\r\n";
			strError += "-dir, /dir" + "\t\t\t" + "Image Directory -- Folder containing images. Specify full path if not using parentDir option.\r\n";
			strError += "-log, /log" + "\t\t\t" + "Log File -- Specify the log file to write to.\r\n";
			strError += "-master, /master" + "\t\t" + "Master Database\r\n";
			strError += "-nopopup, /nopopup" + "\t" + "Suppress any message boxes that pop up during loading. Log will be written instead.\r\n";
			strError += "-autostart, /autostart" + "\t" + "Automatically starts program.\r\n";

			MessageBox.Show(strError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		private void btnStart_Click(object sender, System.EventArgs e)
		{
			clDocuprogram.Start();

			if(boolAutoStart)
				btnExit_Click(this, System.EventArgs.Empty);
		}

		private void btnStop_Click(object sender, System.EventArgs e)
		{
			clDocuprogram.StopProgram();
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
			clDocuprogram = null;
			this.Close();
		}

		public ArrayList GetArrFolder()
		{
			//return a string of entries
			ArrayList arrRet = new ArrayList();
			int i, c;
			c = lstFolderPaths.Items.Count;
			for(i=0; i<c; i++)
			{
				if(lstFolderPaths.GetSelected(i))
				{	
					if(!UtilFile.DirIsThere(lstFolderPaths.Items[i].ToString()))
						clsDocuProgram.Log("ERROR: Folder does not exist: " + lstFolderPaths.Items[i]);
					else
						arrRet.Add(lstFolderPaths.Items[i]);
				}
			}
			
			return arrRet;
		}

		public void ListBoxRemove(String strEntry)
		{
			int i, c;
			c = lstFolderPaths.Items.Count;
			for(i=0; i<c; i++)
			{
				if(lstFolderPaths.Items[i].ToString().ToUpper() == strEntry.ToUpper())
				{	
					lstFolderPaths.Items.RemoveAt(i);
					return;	
				}
			}
		}

		private void Form1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if(e.Data.GetDataPresent(DataFormats.FileDrop, false))
			{
				e.Effect = DragDropEffects.Copy;
			}
			else
			{
				e.Effect = DragDropEffects.None;
			}
		}

		private void Form1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

			foreach(string str in files)
			{
				if(UtilFile.IsDirectory(str))
				{	lstFolderPaths.Items.Add(str);	}
			}
			
			ListBoxSelectAll();
		}

		private void ListBoxSelectAll()
		{
			if(lstFolderPaths.Items.Count <= 0)
			{
				return ;
			}
			for(int i=0; i<lstFolderPaths.Items.Count; i++)
			{
				lstFolderPaths.SetSelected(i, true);
			}
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			bool boolDeselect = false;
			
			if(lstFolderPaths.Items.Count <= 0)
			{
				return ;
			}
			if(lstFolderPaths.GetSelected(0))
			{
				boolDeselect = true;
			}
			for(int i=0; i<lstFolderPaths.Items.Count; i++)
			{
				lstFolderPaths.SetSelected(i, !boolDeselect);
			}
		}

		private void btnRemove_Click(object sender, System.EventArgs e)
		{
			for(int i=lstFolderPaths.Items.Count -1; i>=0; i--)
			{
				if(lstFolderPaths.GetSelected(i))
				{
					lstFolderPaths.Items.RemoveAt(i);
				}
			}
		}

		private void btnViewLog_Click(object sender, System.EventArgs e)
		{
			if(UtilFile.FileIsThere(clsDocuProgram.strLogFile))
			{	
				System.Diagnostics.Process.Start(clsDocuProgram.strLogFile);
				//UtilString.Run("Notepad.exe", clsDocuProgram.strLogFile);	
			}else
			{
				MessageBox.Show("No log found");
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			//set the master db
			openFileDialog1.ShowDialog();
			if(openFileDialog1.FileName != "")
			{
				UtilString.SaveSetting("DocuLoaderX", "Master", openFileDialog1.FileName);
				clsDocuProgram.cOptions.strValMan = openFileDialog1.FileName;
				txtMaster.Text = openFileDialog1.FileName;
				MessageBox.Show("Please restart the program for the changes to take effect");
			}
		}

		private void btnTemplate_Click(object sender, System.EventArgs e)
		{
			//allow option of setting the batch template
			SetTemplateForm frmTemplate = new SetTemplateForm();
			frmTemplate.UseDefault = clsDocuProgram.boolUseDefaultBatchTemplate;
			frmTemplate.BatchTemplate = clsDocuProgram.strCustomBatchTemplate;

			DialogResult dlgRes = frmTemplate.ShowDialog();

			if(dlgRes == DialogResult.OK)
			{
				clsDocuProgram.boolUseDefaultBatchTemplate = frmTemplate.UseDefault;
				if(frmTemplate.UseDefault)
					clsDocuProgram.strCustomBatchTemplate = "";
				else
					clsDocuProgram.strCustomBatchTemplate = frmTemplate.BatchTemplate;
			}
		}

		private void autoStartTimer_Tick(object sender, System.EventArgs e)
		{
			autoStartTimer.Enabled = false;
			btnSelectAll_Click(this, System.EventArgs.Empty);
			btnStart_Click(this, System.EventArgs.Empty);
		}

	}
}
